package uz.infinityandro.foodorder.util

object Constants {
    const val  CARD="card"
    const val FOOD = "food"
    const val ADDRES="address"

    const val FOOD_NEW = "foods"

    const val RESTAURANT = "restaurants"

}