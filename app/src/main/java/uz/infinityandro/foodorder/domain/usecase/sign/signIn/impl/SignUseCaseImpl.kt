package uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl

import uz.infinityandro.foodorder.domain.repository.sign.signIn.SignRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.SignUseCase
import javax.inject.Inject

class SignUseCaseImpl @Inject constructor(
    private val repository: SignRepository
) : SignUseCase {

    override fun checkUser(phone: String, password: String) = repository.checkUser(phone, password)

}