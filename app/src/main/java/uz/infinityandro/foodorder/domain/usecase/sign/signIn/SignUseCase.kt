package uz.infinityandro.foodorder.domain.usecase.sign.signIn

import kotlinx.coroutines.flow.Flow

interface SignUseCase {

    fun checkUser(phone:String,password:String):Flow<Result<Boolean>>
}