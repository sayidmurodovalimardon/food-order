package uz.infinityandro.foodorder.domain.repository.sign.signIn

import kotlinx.coroutines.flow.Flow

interface CodeRepository {
    fun getPhone(): Flow<Result<String>>

}