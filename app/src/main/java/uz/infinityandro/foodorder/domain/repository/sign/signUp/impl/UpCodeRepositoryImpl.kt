package uz.infinityandro.foodorder.domain.repository.sign.signUp.impl

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpCodeRepository
import java.lang.Exception
import javax.inject.Inject

class UpCodeRepositoryImpl @Inject constructor(private val pref: MyPref):UpCodeRepository {
    override fun getPhone(): Flow<Result<String>> = flow{


            val phone1=pref.phone
            emit(Result.success(phone1))

    }
}