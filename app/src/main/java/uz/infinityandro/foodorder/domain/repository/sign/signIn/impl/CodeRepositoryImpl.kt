package uz.infinityandro.foodorder.domain.repository.sign.signIn.impl

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signIn.CodeRepository
import javax.inject.Inject


class CodeRepositoryImpl @Inject constructor(private val pref: MyPref):CodeRepository {
    override fun getPhone(): Flow<Result<String>> = flow{


        val phone1=pref.phone
        emit(Result.success(phone1))

    }
}