package uz.infinityandro.foodorder.domain.usecase.see_all

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.presenter.ui.screens.see_all.Restaurants
import uz.infinityandro.foodorder.util.Resource

interface GetRestaurants {

    operator fun invoke(): Flow<Resource<ArrayList<Restaurant>>>

}