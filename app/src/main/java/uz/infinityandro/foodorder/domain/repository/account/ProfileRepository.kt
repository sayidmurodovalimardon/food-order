package uz.infinityandro.foodorder.domain.repository.account

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.Flow

interface ProfileRepository {
    fun saveDate(image: Bitmap, name:String, email:String, phone:String, password:String): Flow<Result<Boolean>>

    fun getAccountName(): Flow<Result<String>>
    fun getAccountEmail(): Flow<Result<String>>
    fun getAccountImage(): Flow<Result<Bitmap>>
    fun getAccountPassword():Flow<Result<String>>
    fun getAccountPhone():Flow<Result<String>>


}