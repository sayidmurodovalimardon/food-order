package uz.infinityandro.foodorder.domain.repository.sign.signIn.impl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signIn.SignRepository
import javax.inject.Inject

class SignRepositoryImpl @Inject constructor(private val pref: MyPref) : SignRepository {
    override fun checkUser(phone: String, password: String): Flow<Result<Boolean>> = flow {
        try {
            if (phone.equals(pref.phone) && password.equals(pref.password)){
                emit(Result.success(true))
            }else{
                emit(Result.success(false))
            }
        } catch (e: Exception) {

        }
    }
}