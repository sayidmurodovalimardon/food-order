package uz.infinityandro.foodorder.domain.usecase.see_all.imp

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.domain.repository.see_all.MainRepo
import uz.infinityandro.foodorder.domain.usecase.see_all.GetRestaurants
import uz.infinityandro.foodorder.util.Resource
import javax.inject.Inject

class GetRestaurantsImp @Inject constructor(
    private val repo: MainRepo
): GetRestaurants {

    override fun invoke(): Flow<Resource<ArrayList<Restaurant>>> {
        return repo.getRestaurants()
    }
}