package uz.infinityandro.foodorder.domain.repository.sign.signIn.impl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signIn.PhoneRepository
import java.lang.Exception
import javax.inject.Inject

class PhoneRepositoryImpl @Inject constructor(private val pref: MyPref):PhoneRepository {
    override fun savePhone(phone: String): Flow<Result<String>> = flow{
        try {
            pref.phone=phone
            emit(Result.success(phone))
        }catch (e: Exception){

        }
    }
}