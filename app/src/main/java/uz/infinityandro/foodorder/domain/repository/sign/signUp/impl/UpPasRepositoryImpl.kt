package uz.infinityandro.foodorder.domain.repository.sign.signUp.impl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpPasRepository
import javax.inject.Inject


class UpPasRepositoryImpl @Inject constructor(private val pref: MyPref):UpPasRepository {
    override fun savePassword(password: String): Flow<Result<String>> = flow{
        pref.password=password
        emit(Result.success(password))
    }
}