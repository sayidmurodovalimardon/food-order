package uz.infinityandro.foodorder.domain.usecase.sign.signIn

import kotlinx.coroutines.flow.Flow

interface CodeUseCase {
    fun getPhone(): Flow<Result<String>>

}