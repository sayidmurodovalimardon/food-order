package uz.infinityandro.foodorder.domain.usecase.sign.signIn

import kotlinx.coroutines.flow.Flow

interface PhoneUseCase {
    fun savePhone(phone:String): Flow<Result<String>>

}