package uz.infinityandro.foodorder.domain.usecase.account.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.account.AddCardRepository
import uz.infinityandro.foodorder.domain.usecase.account.AddCardUseCase
import javax.inject.Inject

class AddCardUseCaseImpl @Inject constructor(private val repository: AddCardRepository):AddCardUseCase {
    override fun save(number: String, name: String, day: String): Flow<Result<Unit>> =repository.save(number,name,day)
}