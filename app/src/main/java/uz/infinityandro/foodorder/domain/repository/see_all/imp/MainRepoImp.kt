package uz.infinityandro.foodorder.domain.repository.see_all.imp

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import uz.infinityandro.foodorder.data.model.Food
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.domain.repository.see_all.MainRepo
import uz.infinityandro.foodorder.util.Resource
import javax.inject.Inject

class MainRepoImp @Inject constructor(
    private val fireStore: FirebaseFirestore
) : MainRepo, ViewModel() {

    override val foodList: ArrayList<Food> = ArrayList()
    override val restaurantList: ArrayList<Restaurant> = ArrayList()

    override fun getFoodList(): Flow<Resource<ArrayList<Food>>> = flow {

        emit(Resource.Loading())

        var error = ""

        fireStore.collection("foods")
            .get()
            .addOnSuccessListener { result ->
                for (item in result) {
                    item.data.apply {
                        val id = this["id"] as String
                        val name = this["name"] as String
                        val image = this["image"] as String
                        val category = this["category"] as String
                        val isFav = this["_fav"] as Boolean
                        val count = this["count"] as String
                        val cost = this["cost"] as Double
                        val rating = this["rating"] as Double
                        foodList.add(
                            Food(
                                id = id,
                                image = image,
                                name = name,
                                category = category,
                                cost = cost,
                                is_fav = isFav,
                                rating = rating,
                                count = count
                            )
                        )
                        error = ""
                    }
                }

            }
            .addOnFailureListener {
                error = it.message.toString()
            }

        if (error.isNotBlank() && foodList.isEmpty()) {
            emit(
                Resource.Error(
                    message = error
                )
            )
        } else {
            emit(
                Resource.Success(
                    data = foodList
                )
            )
        }


    }


    override fun getRestaurants(): Flow<Resource<ArrayList<Restaurant>>> = flow {
        emit(Resource.Loading())
        var error = ""

        fireStore.collection("restaurants")
            .get()
            .addOnSuccessListener { result ->

                for (item in result) {
                    item.data.apply {
                        val id = this["id"] as String
                        val name = this["name"] as String
                        val image = this["image"] as String
                        val locationName = this["location_name"] as String
                        val longitude = this["long_map"] as Double
                        val lat = this["lat_map"] as Double
                        val isFav = this["_fav"] as Boolean
                        val rating = this["rating"] as Double
                        restaurantList.add(
                            Restaurant(
                                id = id,
                                name = name,
                                image = image,
                                location_name = locationName,
                                long_map = longitude,
                                lat_map = lat,
                                is_fav = isFav,
                                rating = rating
                            )
                        )
                        error = ""
                    }
                }

            }
            .addOnFailureListener {
                error = it.message.toString()
            }

        if (error.isNotBlank() && restaurantList.isEmpty()) {
            emit(
                Resource.Error(
                    message = error
                )
            )
        } else {
            emit(
                Resource.Success(
                    data = restaurantList
                )
            )
        }


    }
}