package uz.infinityandro.foodorder.domain.repository.see_all

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.Food
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.util.Resource

interface MainRepo {

    val foodList: ArrayList<Food>
    val restaurantList: ArrayList<Restaurant>

    fun getFoodList(): Flow<Resource<ArrayList<Food>>>

    fun getRestaurants(): Flow<Resource<ArrayList<Restaurant>>>


}