package uz.infinityandro.foodorder.domain.repository.account

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.card.Card

interface PaymentRepository {
    fun getData():List<Card>
}