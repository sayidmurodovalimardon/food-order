package uz.infinityandro.foodorder.domain.repository.sign.signIn.impl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signIn.PasRepository
import javax.inject.Inject

class PasRepositoryImpl @Inject constructor(private val pref: MyPref):PasRepository {
    override fun savePassword(password: String): Flow<Result<String>> = flow{
        pref.password=password
        emit(Result.success(password))
    }
}