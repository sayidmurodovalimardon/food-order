package uz.infinityandro.foodorder.domain.repository.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpCodeRepository {
    fun getPhone(): Flow<Result<String>>

}