package uz.infinityandro.foodorder.domain.usecase.account

import android.graphics.Bitmap
import kotlinx.coroutines.flow.Flow

interface AccountUseCase {
    fun getAccountName():Flow<Result<String>>
    fun getAccountEmail():Flow<Result<String>>
    fun getAccountImage():Flow<Result<Bitmap>>
}