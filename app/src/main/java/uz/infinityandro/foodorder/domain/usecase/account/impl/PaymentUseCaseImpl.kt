package uz.infinityandro.foodorder.domain.usecase.account.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.card.Card
import uz.infinityandro.foodorder.domain.repository.account.PaymentRepository
import uz.infinityandro.foodorder.domain.usecase.account.PaymentUseCase
import javax.inject.Inject


class PaymentUseCaseImpl @Inject constructor(private val repository: PaymentRepository):PaymentUseCase {
    override fun getAllData() =repository.getData()
}