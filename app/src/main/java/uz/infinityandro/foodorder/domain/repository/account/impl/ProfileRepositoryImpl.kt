package uz.infinityandro.foodorder.domain.repository.account.impl

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.app.App
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.account.ProfileRepository
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.lang.Exception
import javax.inject.Inject

class ProfileRepositoryImpl @Inject constructor(private val pref: MyPref):ProfileRepository {
    override fun saveDate(
        image: Bitmap,
        name: String,
        email: String,
        phone: String,
        password: String
    ): Flow<Result<Boolean>> = flow{

        pref.password=password
        pref.name=name
        pref.email=email
        pref.phone=phone

            val outputStream = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
            val byte = outputStream.toByteArray()
            val image1 = Base64.encodeToString(byte, android.util.Base64.DEFAULT)
           pref.image=image1


        emit(Result.success(true))

    }

    override fun getAccountName(): Flow<Result<String>> = flow{
        try {
            val name=pref.name
            if (name.isNullOrEmpty()){
                emit(Result.success("No Name"))
            }else{
                emit(Result.success(name))
            }

        }catch (e: Exception){

        }
    }

    override fun getAccountEmail(): Flow<Result<String>> = flow{
        try {
            val email=pref.email
            if (email.isNullOrEmpty()){
                emit(Result.success("email"))
            }else{
                emit(Result.success(email))
            }
        }catch (e:Exception){

        }
    }

    override fun getAccountImage(): Flow<Result<Bitmap>> = flow{
        try {
            val image=pref.image
            if (image.isNullOrEmpty()){

            }else{
                val byte=Base64.decode(image,Base64.DEFAULT)
                val bitmap=BitmapFactory.decodeByteArray(byte,0,byte.size)
                emit(Result.success(bitmap))
            }
        }catch (e:Exception){

        }
    }

    override fun getAccountPassword(): Flow<Result<String>> = flow{
        try {
            val password=pref.password
            if (password.isNullOrEmpty()){
                emit(Result.success(" "))
            }else{
                emit(Result.success(password))
            }
        }catch (e:Exception){

        }
    }

    override fun getAccountPhone(): Flow<Result<String>> = flow{
        try {
            val phone=pref.phone
            if (phone.isNullOrEmpty()){
                emit(Result.success(" "))
            }else{
                emit(Result.success(phone))
            }
        }catch (e:Exception){

        }
    }

    private fun getInputStream(image: Uri): InputStream? {
        return App.instance.contentResolver.openInputStream(image)
    }
}