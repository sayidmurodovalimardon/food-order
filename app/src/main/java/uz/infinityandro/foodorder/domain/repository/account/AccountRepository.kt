package uz.infinityandro.foodorder.domain.repository.account

import android.graphics.Bitmap
import kotlinx.coroutines.flow.Flow

interface AccountRepository {
    fun getAccountName(): Flow<Result<String>>
    fun getAccountEmail(): Flow<Result<String>>
    fun getAccountImage(): Flow<Result<Bitmap>>
}