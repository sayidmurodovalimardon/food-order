package uz.infinityandro.foodorder.domain.repository.sign.signIn

import kotlinx.coroutines.flow.Flow

interface PhoneRepository {
    fun savePhone(phone:String): Flow<Result<String>>

}