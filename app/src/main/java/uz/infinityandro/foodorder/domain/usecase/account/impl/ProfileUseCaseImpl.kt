package uz.infinityandro.foodorder.domain.usecase.account.impl

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.account.ProfileRepository
import uz.infinityandro.foodorder.domain.usecase.account.ProfileUseCase
import javax.inject.Inject

class ProfileUseCaseImpl @Inject constructor(private val repository: ProfileRepository):ProfileUseCase {
    override fun saveDate(
        image: Bitmap,
        name: String,
        email: String,
        phone: String,
        password: String
    ): Flow<Result<Boolean>> =repository.saveDate(image,name,email,phone,password)

    override fun getAccountName(): Flow<Result<String>> =repository.getAccountName()

    override fun getAccountEmail(): Flow<Result<String>> =repository.getAccountEmail()

    override fun getAccountImage(): Flow<Result<Bitmap>> =repository.getAccountImage()

    override fun getAccountPassword(): Flow<Result<String>> =repository.getAccountPassword()

    override fun getAccountPhone(): Flow<Result<String>> =repository.getAccountPhone()
}