package uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.sign.signIn.CodeRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.CodeUseCase
import javax.inject.Inject


class CodeUseCaseImpl @Inject constructor(private val repository:CodeRepository):CodeUseCase {
    override fun getPhone(): Flow<Result<String>> =repository.getPhone()

}