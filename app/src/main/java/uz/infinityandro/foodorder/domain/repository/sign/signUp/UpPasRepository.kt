package uz.infinityandro.foodorder.domain.repository.sign.signUp

import java.util.concurrent.Flow

interface UpPasRepository {
    fun savePassword(password:String):kotlinx.coroutines.flow.Flow<Result<String>>
}