package uz.infinityandro.foodorder.domain.usecase.account

import kotlinx.coroutines.flow.Flow

interface AddCardUseCase {
    fun save(number:String,name:String,day:String):Flow<Result<Unit>>
}