package uz.infinityandro.foodorder.domain.repository.account.impl

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.account.AccountRepository
import java.lang.Exception
import java.util.*
import javax.inject.Inject

class AccountRepositoryImpl @Inject constructor(private val pref: MyPref):AccountRepository {
    override fun getAccountName(): Flow<Result<String>> = flow{
        try {
            val name=pref.name
            if (name.isNullOrEmpty()){
                emit(Result.success("No Name"))
            }else{
                emit(Result.success(name))
            }

        }catch (e:Exception){

        }
    }

    override fun getAccountEmail(): Flow<Result<String>> = flow{
        try {
            val email=pref.email
            if (email.isNullOrEmpty()){
                emit(Result.success("email"))
            }else{
                emit(Result.success(email))
            }
        }catch (e:Exception){

        }
    }

    override fun getAccountImage(): Flow<Result<Bitmap>> = flow{
        try {
                val image=pref.image
            if (image.isNullOrEmpty()){

            }else{
                val byte=Base64.decode(image,Base64.DEFAULT)
                val bitmap=BitmapFactory.decodeByteArray(byte,0,byte.size)
                emit(Result.success(bitmap))
            }
        }catch (e:Exception){

        }
    }
}