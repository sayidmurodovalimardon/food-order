package uz.infinityandro.foodorder.domain.usecase.sign.signIn

import kotlinx.coroutines.flow.Flow

interface PasUseCase {
    fun savePassword(password:String): Flow<Result<String>>

}