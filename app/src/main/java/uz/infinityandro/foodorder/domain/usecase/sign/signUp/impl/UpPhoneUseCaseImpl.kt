package uz.infinityandro.foodorder.domain.usecase.sign.signUp.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpPhoneRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpPhoneUseCase
import javax.inject.Inject


class UpPhoneUseCaseImpl @Inject constructor(private val repository:UpPhoneRepository):UpPhoneUseCase{
    override fun savePhone(phone: String): Flow<Result<String>> =repository.savePhone(phone)
}