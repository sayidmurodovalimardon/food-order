package uz.infinityandro.foodorder.domain.repository.sign.signIn

import kotlinx.coroutines.flow.Flow

interface SignRepository {
    fun checkUser(phone:String,password:String):Flow<Result<Boolean>>
}