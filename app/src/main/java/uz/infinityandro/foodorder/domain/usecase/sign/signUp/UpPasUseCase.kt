package uz.infinityandro.foodorder.domain.usecase.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpPasUseCase {
    fun savePassword(password:String): Flow<Result<String>>
}