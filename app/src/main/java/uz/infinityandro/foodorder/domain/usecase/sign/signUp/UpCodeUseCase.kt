package uz.infinityandro.foodorder.domain.usecase.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpCodeUseCase {
    fun getPhone(): Flow<Result<String>>

}