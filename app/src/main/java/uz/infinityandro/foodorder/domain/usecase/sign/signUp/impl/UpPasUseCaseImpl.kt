package uz.infinityandro.foodorder.domain.usecase.sign.signUp.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpPasRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpPasUseCase
import javax.inject.Inject


class UpPasUseCaseImpl @Inject constructor(private val repository:UpPasRepository):UpPasUseCase {
    override fun savePassword(password: String): Flow<Result<String>> =repository.savePassword(password)
}