package uz.infinityandro.foodorder.domain.repository.sign.signUp.impl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpPhoneRepository
import java.lang.Exception
import javax.inject.Inject


class UpPhoneRepositoryImpl @Inject constructor(private val pref: MyPref):UpPhoneRepository {
    override fun savePhone(phone: String): Flow<Result<String>> = flow{
        try {
            pref.phone=phone
            emit(Result.success(phone))
        }catch (e:Exception){

        }
    }
}