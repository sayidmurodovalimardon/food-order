package uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.sign.signIn.PhoneRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.PhoneUseCase
import javax.inject.Inject

class PhoneUseCaseImpl @Inject constructor(private val repository:PhoneRepository):PhoneUseCase {
    override fun savePhone(phone: String): Flow<Result<String>> =repository.savePhone(phone)

}