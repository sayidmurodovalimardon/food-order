package uz.infinityandro.foodorder.domain.repository.account

import kotlinx.coroutines.flow.Flow

interface AddCardRepository {
    fun save(number:String,name:String,day:String):Flow<Result<Unit>>
}