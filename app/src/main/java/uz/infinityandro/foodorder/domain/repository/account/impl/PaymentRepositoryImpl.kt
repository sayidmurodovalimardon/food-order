package uz.infinityandro.foodorder.domain.repository.account.impl

import com.google.firebase.firestore.FirebaseFirestore
import uz.infinityandro.foodorder.data.model.card.Card
import uz.infinityandro.foodorder.domain.repository.account.PaymentRepository
import uz.infinityandro.foodorder.util.Constants
import javax.inject.Inject

class PaymentRepositoryImpl @Inject constructor(private val firebaseDatabase: FirebaseFirestore):PaymentRepository {

    override fun getData():List<Card>{
        var list=ArrayList<Card>()
        firebaseDatabase.collection(Constants.CARD)
            .get()
            .addOnSuccessListener {result->
                result.onEach { document ->
                    val number = document["number"] as String
                    val name = document["name"] as String
                    val expiredDay = document["expiredDay"] as String

                    val data = Card(number, name, expiredDay)

                    list.add(data)

                }
            }
        return list
    }
}