package uz.infinityandro.foodorder.domain.usecase.account

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.card.Card

interface PaymentUseCase {
    fun getAllData():List<Card>
}