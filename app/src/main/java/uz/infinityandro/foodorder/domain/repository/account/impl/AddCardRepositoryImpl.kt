package uz.infinityandro.foodorder.domain.repository.account.impl

import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.infinityandro.foodorder.domain.repository.account.AddCardRepository
import uz.infinityandro.foodorder.util.Constants
import javax.inject.Inject

class AddCardRepositoryImpl @Inject constructor(private val firebaseFirestore: FirebaseFirestore) :
    AddCardRepository {
    override fun save(number: String, name: String, day: String): Flow<Result<Unit>> = flow {
        var hashMap = HashMap<String, String>()
        hashMap.put("number",number)
        hashMap.put("name",name)
        hashMap.put("expiredDay",day)
        firebaseFirestore.collection(Constants.CARD)
            .add(hashMap)
            .addOnSuccessListener {

            }
    }
}