package uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.sign.signIn.PasRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.PasUseCase
import javax.inject.Inject

class PasUseCaseImpl @Inject constructor(private val repository:PasRepository):PasUseCase {
    override fun savePassword(password: String): Flow<Result<String>> =repository.savePassword(password)

}