package uz.infinityandro.foodorder.domain.usecase.account.impl

import android.graphics.Bitmap
import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.account.AccountRepository
import uz.infinityandro.foodorder.domain.usecase.account.AccountUseCase
import javax.inject.Inject

class AccountUseCaseImpl @Inject constructor(private val repository:AccountRepository):AccountUseCase {
    override fun getAccountName(): Flow<Result<String>> =repository.getAccountName()

    override fun getAccountEmail(): Flow<Result<String>> =repository.getAccountEmail()

    override fun getAccountImage(): Flow<Result<Bitmap>> =repository.getAccountImage()
}