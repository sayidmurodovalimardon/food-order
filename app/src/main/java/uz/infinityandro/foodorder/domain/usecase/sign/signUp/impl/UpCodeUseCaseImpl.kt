package uz.infinityandro.foodorder.domain.usecase.sign.signUp.impl

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpCodeRepository
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpCodeUseCase
import javax.inject.Inject


class UpCodeUseCaseImpl @Inject constructor(private val repository:UpCodeRepository):UpCodeUseCase{
    override fun getPhone(): Flow<Result<String>> =repository.getPhone()
}