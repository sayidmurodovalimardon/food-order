package uz.infinityandro.foodorder.domain.repository.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpPhoneRepository {
    fun savePhone(phone:String):Flow<Result<String>>
}