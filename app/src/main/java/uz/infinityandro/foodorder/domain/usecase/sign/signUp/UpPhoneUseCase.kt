package uz.infinityandro.foodorder.domain.usecase.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpPhoneUseCase {
    fun savePhone(phone:String):Flow<Result<String>>
}