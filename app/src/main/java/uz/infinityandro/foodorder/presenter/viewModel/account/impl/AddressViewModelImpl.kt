package uz.infinityandro.foodorder.presenter.viewModel.account.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.data.model.Adress
import uz.infinityandro.foodorder.presenter.viewModel.account.AddressViewModel
import uz.infinityandro.foodorder.util.Constants
import javax.inject.Inject

@HiltViewModel
class AddressViewModelImpl @Inject constructor(private val firestore: FirebaseFirestore):ViewModel(),AddressViewModel {
    override val progress= MutableSharedFlow<Boolean>()
    override val list:ArrayList<Adress> = ArrayList()
    override val data= MutableSharedFlow<List<Adress>>()

    override fun getData() {
        list.clear()
        viewModelScope.launch {
           progress.emit(true)
        }

        firestore.collection(Constants.ADDRES)
            .get()
            .addOnSuccessListener { result->
                result.onEach {document->
                    val adName=document["adName"] as String
                    val long=document["long"] as Double
                    val lan=document["lan"] as Double
                    val name=document["name"] as String
                    val country=document["country"] as String

                    val data=Adress(adName,long,lan,name,country)
                    list.add(data)
                }

                viewModelScope.launch {
                    data.emit(list)
                    progress.emit(false)
                }

            }

    }
}