package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.PageAddressAddBinding
import uz.infinityandro.foodorder.presenter.viewModel.account.AddresAddViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.AddressAddViewModelImpl
import uz.infinityandro.worldnews.utils.showToast

@AndroidEntryPoint
class AddressAddMap:Fragment(R.layout.page_address_add) {
    private val binding by viewBinding(PageAddressAddBinding::bind)
    private val viewModel:AddresAddViewModel by viewModels<AddressAddViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
        init()
        viewModelIsteners()
        combine(
            binding.addressName.textChanges().map {
                it.length>=1
            },
            binding.addressName.textChanges().map {
                it.length>=1
            },
            transform = {addresName,ad->
                ad && addresName
            }
        ).onEach {
            binding.btnAddAdress.isEnabled=it
        }.launchIn(lifecycleScope)

    }

    private fun viewModelIsteners() {
        viewModel.progress.onEach {
            binding.progress.isVisible=it
        }.launchIn(lifecycleScope)

        viewModel.error.onEach {
            showToast(it)
        }.launchIn(lifecycleScope)
        viewModel.success.onEach {
            findNavController().popBackStack()
        }.launchIn(lifecycleScope)
    }

    private fun init() = with(binding){
        val long=arguments?.getDouble("long")
        val lan=arguments?.getDouble("lan")
        val name=arguments?.getString("name")
        val country=arguments?.getString("country")

        var data=HashMap<String,Any>()
        data.put("adName",binding.addressName.text.toString())
        data.put("long", long!!)
        data.put("lan",lan!!)
        data.put("name",name!!)
        data.put("country",country!!)

        address.setText(country)

        binding.btnAddAdress.setOnClickListener {
            viewModel.saveDate(data)
        }

    }

    private fun listeners() {
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}