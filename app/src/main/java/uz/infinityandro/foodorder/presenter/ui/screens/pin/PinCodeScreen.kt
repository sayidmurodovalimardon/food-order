package uz.infinityandro.foodorder.presenter.ui.screens.pin

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.andrognito.pinlockview.PinLockListener
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenPinLockBinding
import uz.infinityandro.foodorder.presenter.viewModel.pin.PinViewModel
import uz.infinityandro.foodorder.presenter.viewModel.pin.impl.PinViewModelImpl
import uz.infinityandro.worldnews.utils.showToast

@AndroidEntryPoint
class PinCodeScreen:Fragment(R.layout.screen_pin_lock) {
    private val binding by viewBinding(ScreenPinLockBinding::bind)
    private val viewModel:PinViewModel by viewModels<PinViewModelImpl>()
    var code=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).postDelayed(object :Runnable{
            override fun run() {
                viewModel.getPref()
            }

        },10)


        viewModel.sharedPrefFlow.onEach {
            code=it
        }.launchIn(lifecycleScope)


        binding.pinLock.setPinLockListener(mPinLock)
        binding.pinLock.attachIndicatorDots(binding.indicator)

    }


    var mPinLock=object :PinLockListener{
        override fun onComplete(pin: String?) {
            if (code==pin){
                findNavController().navigate(R.id.action_pinCodeScreen_to_mainScreen)
            }else{
                showToast("Invalid")
            }
        }

        override fun onEmpty() {

        }

        override fun onPinChange(pinLength: Int, intermediatePin: String?) {
        }

    }
}