package uz.infinityandro.foodorder.presenter.viewModel.account

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.Flow

interface ProfileViewModel {
    val accountName:Flow<String>
    val accountEmail:Flow<String>
    val accountImage:Flow<Bitmap>
    val accountPhone:Flow<String>
    val accountPassword:Flow<String>
    val progressFlow:Flow<Boolean>

    fun getAccountInformation()
    fun saveDate(image:Bitmap,name:String,email:String,phone:String,password:String)



}