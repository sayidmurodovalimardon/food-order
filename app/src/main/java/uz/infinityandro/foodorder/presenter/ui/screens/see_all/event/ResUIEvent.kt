package uz.infinityandro.foodorder.presenter.ui.screens.see_all.event

sealed class ResUIEvent {
    data class ShowToast(val message: String): ResUIEvent()
}