package uz.infinityandro.foodorder.presenter.viewModel.account.impl

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.domain.usecase.account.AccountUseCase
import uz.infinityandro.foodorder.presenter.viewModel.account.AccountViewModel
import javax.inject.Inject

@HiltViewModel
class AccountViewModelImpl @Inject constructor(private val useCase:AccountUseCase)  :ViewModel(),AccountViewModel{

    override val accountName= MutableSharedFlow<String>()
    override val accountEmail= MutableSharedFlow<String>()
    override val accountImage= MutableSharedFlow<Bitmap>()

    init {
        getAccountInformation()
    }


    override fun getAccountInformation() {
        useCase.getAccountName().onEach {
            it.onSuccess {
                accountName.emit(it)
            }
        }.launchIn(viewModelScope)
        useCase.getAccountEmail().onEach {
            it.onSuccess {
                accountEmail.emit(it)
            }
        }.launchIn(viewModelScope)

        useCase.getAccountImage().onEach {
            it.onSuccess {
                accountImage.emit(it)
            }
        }.launchIn(viewModelScope)
    }
}