package uz.infinityandro.foodorder.presenter.ui.screens.see_all

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenRestaurantsBinding
import uz.infinityandro.foodorder.presenter.ui.adapter.ResAdapter
import uz.infinityandro.foodorder.presenter.ui.screens.see_all.event.ResUIEvent
import uz.infinityandro.foodorder.presenter.viewModel.account.AccountViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.AccountViewModelImpl
import uz.infinityandro.foodorder.presenter.viewModel.see_all.ResVM
import uz.infinityandro.foodorder.presenter.viewModel.see_all.imp.ResVMImp
import uz.infinityandro.worldnews.utils.showToast

@AndroidEntryPoint
class Restaurants: Fragment(R.layout.screen_restaurants) {
    private val binding by viewBinding(ScreenRestaurantsBinding::bind)
    private val viewModel: ResVM by viewModels<ResVMImp>()
    private lateinit var adapter: ResAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeToChanges()

        subscribeToEvents()

    }

    private fun subscribeToChanges() {
        val state = viewModel.state.value
        if (state.isLoading) binding.progressBar.visibility = View.VISIBLE else binding.progressBar.visibility = View.GONE
        adapter = ResAdapter(state.resList)
        binding.rvRestaurants.layoutManager = LinearLayoutManager(requireContext())
        binding.rvRestaurants.adapter = adapter
    }

    private fun subscribeToEvents() {
        lifecycleScope.launch {
            viewModel.event.collectLatest { event ->
                when(event) {
                    is ResUIEvent.ShowToast -> showToast(message = event.message)
                }
            }
        }
    }



}