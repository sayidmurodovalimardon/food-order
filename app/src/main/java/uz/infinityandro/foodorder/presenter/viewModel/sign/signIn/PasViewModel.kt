package uz.infinityandro.foodorder.presenter.viewModel.sign.signIn

import kotlinx.coroutines.flow.Flow

interface PasViewModel {
    val openScreen: Flow<Unit>
    fun savePassword(password:String)
}