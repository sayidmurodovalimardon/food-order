package uz.infinityandro.foodorder.presenter.viewModel.main

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.enums.BottomPage

interface MainViewModel {
    val openSelectPosPageFlow: Flow<BottomPage>
    val returnPage:Flow<Int>
    fun selectPagePos(page : BottomPage)
    fun openPage()
}