package uz.infinityandro.foodorder.presenter.viewModel.account

import android.graphics.Bitmap
import kotlinx.coroutines.flow.Flow

interface AccountViewModel {

    val accountName:Flow<String>
    val accountEmail:Flow<String>
    val accountImage:Flow<Bitmap>


    fun getAccountInformation()

}