package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.data.model.Adress
import uz.infinityandro.foodorder.data.model.card.Card
import uz.infinityandro.foodorder.databinding.PageAddressBinding
import uz.infinityandro.foodorder.presenter.ui.adapter.address.AdapterLocation
import uz.infinityandro.foodorder.presenter.viewModel.account.AddressViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.AddressViewModelImpl

@AndroidEntryPoint
class AddressScreen:Fragment(R.layout.page_address) {
    private val binding by viewBinding(PageAddressBinding::bind)
    private val viewModel:AddressViewModel by viewModels<AddressViewModelImpl>()
    val list = ArrayList<Adress>()
    lateinit var adapter:AdapterLocation


    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding){
        super.onViewCreated(view, savedInstanceState)

        viewModel.getData()
        adapter= AdapterLocation(list)
        recycler.adapter=adapter
        binding.recycler.layoutManager=LinearLayoutManager(requireContext())
        otherLocation.setOnClickListener {
            findNavController().navigate(R.id.action_addressScreen_to_addressMap)
        }
        btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
        viewModelListeners()
    }

    private fun viewModelListeners() = with(binding){
        viewModel.progress.onEach {
            progress.isVisible=it
        }.launchIn(lifecycleScope)

        viewModel.data.onEach {
            list.clear()
            list.addAll(it)
            adapter.notifyDataSetChanged()

        }.launchIn(lifecycleScope)
    }
}