package uz.infinityandro.foodorder.presenter.viewModel.account.impl

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.domain.usecase.account.ProfileUseCase
import uz.infinityandro.foodorder.presenter.viewModel.account.ProfileViewModel
import java.io.ByteArrayOutputStream
import javax.inject.Inject

@HiltViewModel
class ProfileViewModelImpl @Inject constructor(
    private val useCase: ProfileUseCase,
    @ApplicationContext private val context: Context
) : ViewModel(), ProfileViewModel {
    override val accountName = MutableSharedFlow<String>()
    override val accountEmail = MutableSharedFlow<String>()
    override val accountImage = MutableSharedFlow<Bitmap>()
    override val accountPhone = MutableSharedFlow<String>()
    override val accountPassword = MutableSharedFlow<String>()
    override val progressFlow = MutableSharedFlow<Boolean>()

    init {
        getAccountInformation()
    }


    override fun getAccountInformation() {

        useCase.getAccountEmail().onEach {
            it.onSuccess {
                accountEmail.emit(it)
            }

        }.launchIn(viewModelScope)

        useCase.getAccountImage().onEach {
            it.onSuccess {
                accountImage.emit(it)
            }
        }.launchIn(viewModelScope)

        useCase.getAccountName().onEach {
            it.onSuccess {
                accountName.emit(it)
            }
        }.launchIn(viewModelScope)

        useCase.getAccountPassword().onEach {
            it.onSuccess {
                accountPassword.emit(it)
            }
        }.launchIn(viewModelScope)

        useCase.getAccountPhone().onEach {
            it.onSuccess {
                accountPhone.emit(it)
            }
        }.launchIn(viewModelScope)
    }

    fun getImageUriFromBitmap(context: Context, bitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        return Uri.parse(bitmap.toString())

    }

    override fun saveDate(
        image: Bitmap,
        name: String,
        email: String,
        phone: String,
        password: String
    ) {
        useCase.saveDate(image, name, email, phone, password).onEach {
            progressFlow.emit(true)
            it.onSuccess {
                if (it) {
                    progressFlow.emit(false)
                }
            }
        }.launchIn(viewModelScope)
    }
}