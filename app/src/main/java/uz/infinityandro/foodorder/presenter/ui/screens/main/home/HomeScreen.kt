package uz.infinityandro.foodorder.presenter.ui.screens.main.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.data.model.Food
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.databinding.HomeScreenBinding
import uz.infinityandro.foodorder.databinding.ScreenHomeBinding
import uz.infinityandro.foodorder.presenter.ui.adapter.FoodAdapter
import uz.infinityandro.foodorder.presenter.ui.adapter.SmallRestauranAdapter
import uz.infinityandro.foodorder.presenter.viewModel.home.HomeViewModel
import uz.infinityandro.foodorder.presenter.viewModel.home.impl.HomeViewModelImpl

@AndroidEntryPoint
class HomeScreen:Fragment(R.layout.home_screen) {
    private val binding by viewBinding(HomeScreenBinding::bind)
    private val viewModel:HomeViewModel by viewModels<HomeViewModelImpl>()
    private var  list=ArrayList<Restaurant>()
    private var  listFod=ArrayList<Food>()
    private lateinit var adapter: SmallRestauranAdapter
    private lateinit var adapterFood:FoodAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding){
        super.onViewCreated(view, savedInstanceState)


        adapter=SmallRestauranAdapter(list){

        }
        rvPopularRestaurants.adapter=adapter
        adapterFood= FoodAdapter(listFod){

        }
        rvPopularFoods.adapter=adapterFood
        viewModelListeners()
        viewModel.getRestaran()
        viewModel.getFoods()

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun viewModelListeners() = with(binding){
        viewModel.progressRestaran.onEach {
            progressRestaurans.isVisible=it
        }.launchIn(lifecycleScope)

        viewModel.restaran.onEach {
            list.clear()
            list.addAll(it)
            adapter.notifyDataSetChanged()
        }.launchIn(lifecycleScope)

        viewModel.progressFood.onEach {
            progressFod.isVisible=it
        }.launchIn(lifecycleScope)

        viewModel.food.onEach {
            listFod.clear()
            listFod.addAll(it)
            adapterFood.notifyDataSetChanged()
        }.launchIn(lifecycleScope)
    }
}