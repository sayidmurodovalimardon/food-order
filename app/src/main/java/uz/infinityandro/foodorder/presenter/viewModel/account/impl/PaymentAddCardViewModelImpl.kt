package uz.infinityandro.foodorder.presenter.viewModel.account.impl

import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.app.App
import uz.infinityandro.foodorder.domain.usecase.account.AddCardUseCase
import uz.infinityandro.foodorder.presenter.viewModel.account.PaymentAddCardViewModel
import javax.inject.Inject

@HiltViewModel
class PaymentAddCardViewModelImpl @Inject constructor(private val useCase: AddCardUseCase):ViewModel(),PaymentAddCardViewModel {
    override fun save(number: String, name: String, day: String) {
        useCase.save(number,name,day).onEach{
            it.onSuccess {
                Toast.makeText(App.instance, "Saved", Toast.LENGTH_SHORT).show()
            }
        }.launchIn(viewModelScope)
    }
}