package uz.infinityandro.foodorder.presenter.viewModel.main.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.data.enums.BottomPage
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.presenter.viewModel.main.MainViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModelImpl @Inject constructor(private val pref: MyPref) : ViewModel(), MainViewModel {
    override val openSelectPosPageFlow = MutableSharedFlow<BottomPage>()
    override val returnPage = MutableSharedFlow<Int>()
    private var selectPos = 0

    override fun selectPagePos(page: BottomPage) {
        if (selectPos != page.pos) {
            selectPos = page.pos
            viewModelScope.launch {
                openSelectPosPageFlow.emit(page)
            }
        }
    }

    override fun openPage() {
        viewModelScope.launch {
            returnPage.emit(pref.pos)
        }
    }
}