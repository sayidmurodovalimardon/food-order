package uz.infinityandro.foodorder.presenter.ui.screens.payment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenCardBinding

@AndroidEntryPoint
class AddCardScreen:Fragment(R.layout.screen_card) {
    private val binding by viewBinding(ScreenCardBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}