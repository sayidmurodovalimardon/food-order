package uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.CodeUseCase
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.CodeViewModel
import javax.inject.Inject

@HiltViewModel
class CodeViewModelImpl @Inject constructor(private val useCase:CodeUseCase) :ViewModel(),CodeViewModel {
    override val backFlow= MutableSharedFlow<Unit>()
    override val phoneNumberFlow= MutableSharedFlow<String>()

    override fun backFun() {
        viewModelScope.launch {
            backFlow.emit(Unit)
        }
    }

    override fun getPhoneNumber() {
        useCase.getPhone().onEach {
            it.onSuccess { phone->
                phoneNumberFlow.emit(phone)
            }.onFailure {

            }
        }.launchIn(viewModelScope)
    }
}