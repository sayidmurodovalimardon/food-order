package uz.infinityandro.foodorder.presenter.ui.screens.sign.signUp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenSignUpSetPasswordBinding
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.UpPasViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.impl.UpPasViewModelImpl

@AndroidEntryPoint
class SignUpPassword:Fragment(R.layout.screen_sign_up_set_password) {
    private val binding by viewBinding(ScreenSignUpSetPasswordBinding::bind)
    private val viewModel:UpPasViewModel by viewModels<UpPasViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding){
        super.onViewCreated(view, savedInstanceState)
        combine(
            etPhoneNumber.textChanges().map {
                it.length==4
            },
            etPassword.textChanges().map {
                it.length==4
            },
            transform = {etPhoneNumber,etPassword->
                etPhoneNumber && etPassword && etPhoneNumber==etPassword
            }
        ).onEach {
            btConfirm.isEnabled=it
        }.launchIn(lifecycleScope)
        listeners()
        viewModelisteners()
    }

    private fun viewModelisteners() {
        viewModel.openScreen.onEach {
            findNavController().navigate(R.id.pinCodeScreen)
        }.launchIn(lifecycleScope)
    }

    private fun listeners() = with(binding){
        btConfirm.setOnClickListener {
            if (etPassword.text.toString().equals(etPhoneNumber.text.toString())){
                viewModel.savePassword(etPhoneNumber.text.toString())
            }
        }
    }
}