package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.data.model.card.Card
import uz.infinityandro.foodorder.databinding.PagePaymentManegBinding
import uz.infinityandro.foodorder.presenter.ui.adapter.address.AdapterCards
import uz.infinityandro.foodorder.presenter.viewModel.account.PaymentViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.PaymentViewModelImpl
import uz.infinityandro.worldnews.utils.showToast

@AndroidEntryPoint
class PaymentManegScreen : Fragment(R.layout.page_payment_maneg) {
    private val binding by viewBinding(PagePaymentManegBinding::bind)
    private val viewModel: PaymentViewModel by viewModels<PaymentViewModelImpl>()
    lateinit var adapter: AdapterCards
    val list = ArrayList<Card>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = AdapterCards(list)
        binding.recycler.adapter = adapter
        listeners()
        viewModelListeners()
        viewModel.getData()

    }
    private fun viewModelListeners() {
            viewModel.cards.onEach {
                list.clear()
                list.addAll(it)
                adapter.notifyDataSetChanged()
            }.launchIn(lifecycleScope)
        viewModel.progress.onEach {
            binding.cardVisible.isVisible=it
        }.launchIn(lifecycleScope)
    }

    private fun listeners() = with(binding) {
        btnAddCard.setOnClickListener {
            findNavController().navigate(R.id.paymentCardAdd)
        }

        btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}