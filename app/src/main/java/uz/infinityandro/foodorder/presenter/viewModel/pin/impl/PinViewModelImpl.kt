package uz.infinityandro.foodorder.presenter.viewModel.pin.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.presenter.viewModel.pin.PinViewModel
import javax.inject.Inject

@HiltViewModel
class PinViewModelImpl @Inject constructor(private val pref: MyPref):ViewModel(),PinViewModel{
    override val sharedPrefFlow= MutableSharedFlow<String>()

    override fun getPref() {
        viewModelScope.launch {
            val password=pref.password
            sharedPrefFlow.emit(password)
        }
    }
}