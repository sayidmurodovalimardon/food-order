package uz.infinityandro.foodorder.presenter.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.databinding.ItemRestaurantsBinding

class ResAdapter(
    private val list: ArrayList<Restaurant>
): RecyclerView.Adapter<ResAdapter.VH>(){

    inner class VH(val binding: ItemRestaurantsBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = ItemRestaurantsBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        with(holder) {
            with(list[position]) {
                var isFav = if(this.is_fav) R.drawable.ic_fav_filled else R.drawable.ic_fav_empty
                Glide.with(binding.resImage).load(this.image).into(binding.resImage)
                binding.resName.text = this.name
                binding.resLocation.text = this.location_name
                binding.ivFav.setImageResource(isFav)
                binding.ratingFood.text = "${this.rating}"
            }
        }
    }

    override fun getItemCount(): Int = list.size
}