package uz.infinityandro.foodorder.presenter.viewModel.onBoarding.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.presenter.viewModel.onBoarding.BoardViewModel
import javax.inject.Inject

@HiltViewModel
class BoardViewModelImpl @Inject constructor() :ViewModel(),BoardViewModel {
    override val openLoginScreenFlow= MutableSharedFlow<Boolean>()

    override fun open() {
        viewModelScope.launch {
            openLoginScreenFlow.emit(true)
        }
    }


}