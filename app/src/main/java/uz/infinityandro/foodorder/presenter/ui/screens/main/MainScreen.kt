package uz.infinityandro.foodorder.presenter.ui.screens.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.data.enums.BottomPage
import uz.infinityandro.foodorder.databinding.ScreenMainBinding
import uz.infinityandro.foodorder.presenter.ui.adapter.AdapterMainScreen
import uz.infinityandro.foodorder.presenter.viewModel.main.MainViewModel
import uz.infinityandro.foodorder.presenter.viewModel.main.impl.MainViewModelImpl

@AndroidEntryPoint
class MainScreen : Fragment(R.layout.screen_main) {
    private val binding by viewBinding(ScreenMainBinding::bind)
    private val viewModel: MainViewModel by viewModels<MainViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = AdapterMainScreen(childFragmentManager, lifecycle)
        viewPager.adapter = adapter
        viewPager.isUserInputEnabled = false

        bottomNav.setOnItemSelectedListener {

            when (it.itemId) {
                R.id.home -> {
                    viewModel.selectPagePos(BottomPage.HOME)
                }
                R.id.like -> {
                    viewModel.selectPagePos(BottomPage.LIKE)
                }
                R.id.card -> {
                    viewModel.selectPagePos(BottomPage.CARD)
                }
                R.id.account -> {
                    viewModel.selectPagePos(BottomPage.ACCOUNT)
                }
            }
            return@setOnItemSelectedListener true
        }
        viewModel.openSelectPosPageFlow.onEach {
            viewPager.currentItem = it.pos
        }.launchIn(lifecycleScope)

    }


}