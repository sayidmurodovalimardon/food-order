package uz.infinityandro.foodorder.presenter.ui.screens.sign.signIn

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenResetCodeBinding
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.CodeViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.impl.CodeViewModelImpl
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.UpCodeViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.impl.UpCodeViewModelImpl
import uz.infinityandro.worldnews.utils.showToast

@AndroidEntryPoint
class ResetPasswordCode : Fragment(R.layout.screen_reset_code) {
    private val binding by viewBinding(ScreenResetCodeBinding::bind)
    private val viewModel: CodeViewModel by viewModels<CodeViewModelImpl>()
    private var firebaseAuth = FirebaseAuth.getInstance()
    var verificationId = ""
    val code = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        verificationId = arguments?.getString("key").toString()
        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
            override fun run() {
                viewModel.getPhoneNumber()
            }

        }, 10)
        combine(
            pinView.textChanges().map {
                it.length == 6
            },
            pinView.textChanges().map {
                it.length == 6
            },
            transform = { pinView, ssasa ->
                pinView && ssasa
            }
        ).onEach {
            btConfirm.isEnabled = it
        }.launchIn(lifecycleScope)

        listeners()
        viewModelisteners()


    }

    private fun viewModelisteners() = with(binding) {
        viewModel.backFlow.onEach {
            findNavController().popBackStack()
        }.launchIn(lifecycleScope)

        viewModel.phoneNumberFlow.onEach {
            phoneNumber.setText(it.toString())
        }.launchIn(lifecycleScope)
    }

    private fun listeners() = with(binding) {
        btConfirm.setOnClickListener {
            val code = pinView.text.toString().trim()
            verfyCode(verificationId, code)
        }
        btBack.setOnClickListener {
            viewModel.backFun()
        }
    }

    private fun verfyCode(verificationId: String, code: String) {
        val credential = PhoneAuthProvider.getCredential(verificationId, code)

        otpVerify(credential)
    }

    private fun otpVerify(credential: PhoneAuthCredential) {
        firebaseAuth
            .signInWithCredential(credential)
            .addOnCompleteListener {

            }
            .addOnFailureListener {
                showToast(it.localizedMessage)

            }.addOnSuccessListener {
                findNavController().navigate(R.id.resetPasswordScreen)
            }

    }


}