package uz.infinityandro.foodorder.presenter.ui.screens.see_all.state

import uz.infinityandro.foodorder.data.model.Restaurant

data class ResInfoState(
    val resList: ArrayList<Restaurant> = ArrayList(),
    val isLoading: Boolean = false
)