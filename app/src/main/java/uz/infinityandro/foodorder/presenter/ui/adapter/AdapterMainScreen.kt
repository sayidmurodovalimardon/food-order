package uz.infinityandro.foodorder.presenter.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import uz.infinityandro.foodorder.presenter.ui.screens.account.AccountScreen
import uz.infinityandro.foodorder.presenter.ui.screens.main.home.HomeScreen
import uz.infinityandro.foodorder.presenter.ui.screens.main.liked.LikedScreen
import uz.infinityandro.foodorder.presenter.ui.screens.payment.AddCardScreen

class AdapterMainScreen(
    private val fm: FragmentManager,
    private val lifecycle: Lifecycle
): FragmentStateAdapter(fm, lifecycle)  {
    override fun getItemCount(): Int {
        return  4
    }
//
    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> {
                val homeScreen=HomeScreen()
                homeScreen
            }
            1 -> {
                val likeScreen=LikedScreen()
                likeScreen
            }
            2 -> {
                val addScr=AddCardScreen()
                addScr
            }
            else -> {
                val accountScreen=AccountScreen()
                accountScreen
            }
        }
    }

}