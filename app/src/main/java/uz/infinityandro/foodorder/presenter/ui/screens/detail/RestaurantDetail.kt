package uz.infinityandro.foodorder.presenter.ui.screens.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenRestaurantDetailBinding

@AndroidEntryPoint
class RestaurantDetail: Fragment(R.layout.screen_restaurant_detail) {
    private val binding by viewBinding(ScreenRestaurantDetailBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}