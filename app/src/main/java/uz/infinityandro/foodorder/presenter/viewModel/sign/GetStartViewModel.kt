package uz.infinityandro.foodorder.presenter.viewModel.sign

import kotlinx.coroutines.flow.Flow

interface GetStartViewModel {
    val createAccountFlow:Flow<Unit>
    val signInFlow:Flow<Unit>
    val googleOpenFlow:Flow<Unit>

    fun accountScreen()
    fun signInScreen()
    fun googleScreen(name:String,email:String)
}