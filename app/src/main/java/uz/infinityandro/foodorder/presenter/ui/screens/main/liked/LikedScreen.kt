package uz.infinityandro.foodorder.presenter.ui.screens.main.liked

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenLikeBinding

@AndroidEntryPoint
class LikedScreen:Fragment(R.layout.screen_like) {
    private val binding by viewBinding(ScreenLikeBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}