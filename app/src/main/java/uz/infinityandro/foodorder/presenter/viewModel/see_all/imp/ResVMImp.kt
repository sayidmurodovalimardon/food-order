package uz.infinityandro.foodorder.presenter.viewModel.see_all.imp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.domain.usecase.see_all.GetRestaurants
import uz.infinityandro.foodorder.presenter.ui.screens.see_all.event.ResUIEvent
import uz.infinityandro.foodorder.presenter.ui.screens.see_all.state.ResInfoState
import uz.infinityandro.foodorder.presenter.viewModel.see_all.ResVM
import uz.infinityandro.foodorder.util.Resource
import javax.inject.Inject


@HiltViewModel
class ResVMImp @Inject constructor(
    private val getRestaurants: GetRestaurants
): ResVM,ViewModel() {
    private val _state = MutableStateFlow(ResInfoState())
    private val _event = MutableSharedFlow<ResUIEvent>()


    override val state: StateFlow<ResInfoState> = _state
    override val event: SharedFlow<ResUIEvent> = _event.asSharedFlow()


    init {
        initResInfo()
    }

    private fun initResInfo() {
        viewModelScope.launch {
            getRestaurants().onEach { result ->
                when (result) {
                    is Resource.Success ->{
                        _state.value = state.value.copy(
                            resList = result.data ?: ArrayList(),
                            isLoading = false
                        )
                    }

                    is Resource.Error ->{
                        _state.value = state.value.copy(
                            resList = result.data ?: ArrayList(),
                            isLoading = false
                        )

                        _event.emit(ResUIEvent.ShowToast(
                            result.message ?: "Unknown error occurred!"
                        ))

                    }

                    is Resource.Loading ->{
                        _state.value = state.value.copy(
                            resList = result.data ?: ArrayList(),
                            isLoading = true
                        )
                    }



                }

            }.launchIn(this)
        }
    }

}