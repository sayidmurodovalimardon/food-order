package uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpPhoneUseCase
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.UpPhoneViewModel
import javax.inject.Inject

@HiltViewModel
class UpPhoneViewModelImpl @Inject constructor(private val useCase:UpPhoneUseCase) :ViewModel(),UpPhoneViewModel {
    override val backFlow= MutableSharedFlow<Unit>()
    override val singInFlow= MutableSharedFlow<Unit>()
    override val progressFlow= MutableSharedFlow<Boolean>()
    override val phoneNumberFlow= MutableSharedFlow<String>()

    override fun backFun() {
        viewModelScope.launch {
            backFlow.emit(Unit)
        }
    }

    override fun singInScreen() {
        viewModelScope.launch {
            singInFlow.emit(Unit)
        }
    }

    override fun savePhoneNumber(phone: String) {
        useCase.savePhone(phone).onEach {
            progressFlow.emit(true)
            it.onSuccess {
                progressFlow.emit(false)
                phoneNumberFlow.emit(it)
            }.onFailure {
                progressFlow.emit(false)
            }
        }.launchIn(viewModelScope)
    }
}