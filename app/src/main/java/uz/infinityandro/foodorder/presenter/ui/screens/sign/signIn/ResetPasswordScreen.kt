package uz.infinityandro.foodorder.presenter.ui.screens.sign.signIn

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenResetSetNewPasswordBinding
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.PasViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.impl.PasViewModelImpl

@AndroidEntryPoint
class ResetPasswordScreen:Fragment(R.layout.screen_reset_set_new_password) {
    private val binding by viewBinding(ScreenResetSetNewPasswordBinding::bind)
    private val viewModel:PasViewModel by viewModels<PasViewModelImpl>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding){
        super.onViewCreated(view, savedInstanceState)
        combine(
            etPhoneNumber.textChanges().map {
                it.length==4
            },
            etPassword.textChanges().map {
                it.length==4
            },
            transform = {etPhoneNumber,etPassword->
                etPhoneNumber && etPassword && etPhoneNumber==etPassword
            }
        ).onEach {
            btConfirm.isEnabled=it
        }.launchIn(lifecycleScope)
        listeners()
        viewModelisteners()

    }

    private fun viewModelisteners() {
        viewModel.openScreen.onEach {
            findNavController().navigate(R.id.pinCodeScreen)
        }.launchIn(lifecycleScope)
    }

    private fun listeners() = with(binding){
        btConfirm.setOnClickListener {
            if (etPassword.text.toString().equals(etPhoneNumber.text.toString())){
                viewModel.savePassword(etPhoneNumber.text.toString())
            }
        }
    }
}