package uz.infinityandro.foodorder.presenter.ui.screens.sign.signUp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenSignUpPhoneBinding
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.UpPhoneViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.impl.UpPhoneViewModelImpl
import uz.infinityandro.worldnews.utils.showToast
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class SignUpPhone : Fragment(R.layout.screen_sign_up_phone) {
    private val binding by viewBinding(ScreenSignUpPhoneBinding::bind)
    private val viewModel: UpPhoneViewModel by viewModels<UpPhoneViewModelImpl>()
    private var firebaseAuth = FirebaseAuth.getInstance()
    private var mCallBacks : PhoneAuthProvider.OnVerificationStateChangedCallbacks?=null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)


          mCallBacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {

            }

            override fun onVerificationFailed(p0: FirebaseException) {

            }

            override fun onCodeSent(id: String, p1: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(id, p1)
                binding.btConfirm.visibility = View.VISIBLE
                binding.progress.visibility = View.GONE
                val fragment=SignUpCode()
                var bundel=Bundle()
                bundel.putString("key",id)
                fragment.arguments=bundel
                findNavController().navigate(R.id.signUpCode,bundel)
            }

        }
        combine(
            etPhoneNumber.textChanges().map {
                it.length == 19
            },
            etPhoneNumber.textChanges().map {
                it.length == 19
            },
            transform = { etPhoneNumber, salom ->
                etPhoneNumber && salom
            }
        ).onEach {
            btConfirm.isEnabled = it
        }.launchIn(lifecycleScope)
        listeners()
        viewModelListeners()
    }

    private fun viewModelListeners() {
        viewModel.backFlow.onEach {
            findNavController().navigate(R.id.getStartedScreen)
        }.launchIn(lifecycleScope)

        viewModel.singInFlow.onEach {
            findNavController().navigate(R.id.signInScreen)
        }.launchIn(lifecycleScope)

        viewModel.phoneNumberFlow.onEach {
            showToast(it)
            options(it)
        }.launchIn(lifecycleScope)

    }

    private fun listeners() = with(binding) {

        btBack.setOnClickListener {
            viewModel.backFun()
        }

        btConfirm.setOnClickListener {
            val phone = "+998${etPhoneNumber.rawText.trim()}"
            viewModel.savePhoneNumber(phone)
            btConfirm.visibility = View.GONE
            progress.visibility = View.VISIBLE
        }

        singIn.setOnClickListener {
            viewModel.singInScreen()

        }
    }


    private fun options(phone: String) {
        val options = PhoneAuthOptions.newBuilder(firebaseAuth)
            .setPhoneNumber(phone)
            .setTimeout(30L, TimeUnit.SECONDS)
            .setActivity(requireActivity())
            .setCallbacks(mCallBacks!!)
            .build()

        PhoneAuthProvider.verifyPhoneNumber(options)
    }

}