package uz.infinityandro.foodorder.presenter.viewModel.account

import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.card.Card

interface PaymentViewModel {
    val cards:Flow<List<Card>>
    val list:List<Card>
    val progress:Flow<Boolean>
    val toCard:Flow<Unit>
    fun getData()
    fun toFlow()

}