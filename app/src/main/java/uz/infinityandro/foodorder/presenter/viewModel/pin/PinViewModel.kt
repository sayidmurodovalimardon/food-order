package uz.infinityandro.foodorder.presenter.viewModel.pin

import kotlinx.coroutines.flow.Flow

interface PinViewModel {
    val sharedPrefFlow:Flow<String>
    fun getPref()
}