package uz.infinityandro.foodorder.presenter.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.foodorder.data.enums.Start
import uz.infinityandro.foodorder.databinding.ItemGetStartBinding

class AdapterOnBoard : RecyclerView.Adapter<AdapterOnBoard.VH>() {
    inner class VH(val binding: ItemGetStartBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) = with(binding) {
            val entrance = Start.values().get(position)
//            image.setImageResource(entrance.image)
            title.setText(entrance.title)
            desc.setText(entrance.description)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemGetStartBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return Start.values().size
    }
}