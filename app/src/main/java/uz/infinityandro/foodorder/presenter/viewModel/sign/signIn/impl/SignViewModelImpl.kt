package uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.SignUseCase
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.SignViewModel
import javax.inject.Inject

@HiltViewModel
class SignViewModelImpl @Inject constructor(private val useCase:SignUseCase) :ViewModel(),SignViewModel {
    override val backFlow= MutableSharedFlow<Unit>()
    override val accountNew= MutableSharedFlow<Unit>()
    override val progress= MutableSharedFlow<Boolean>()
    override val resetPassword= MutableSharedFlow<Unit>()
    override val checkUser= MutableSharedFlow<Boolean>()

    override fun back() {
        viewModelScope.launch {
            backFlow.emit(Unit)
        }
    }

    override fun newAccount() {
        viewModelScope.launch {
            accountNew.emit(Unit)
        }
    }

    override fun resetPassword() {
        viewModelScope.launch {
            resetPassword.emit(Unit)
        }
    }

    override fun checkUser(phone: String, password: String) {
        useCase.checkUser(phone,password).onEach{
            progress.emit(true)
            it.onSuccess {
                progress.emit(false)
                checkUser.emit(it)

            }.onFailure {

            }
        }.launchIn(viewModelScope)
    }
}