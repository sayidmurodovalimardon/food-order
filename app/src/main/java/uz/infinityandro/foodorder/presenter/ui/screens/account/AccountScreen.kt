package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenAccountBinding
import uz.infinityandro.foodorder.presenter.viewModel.account.AccountViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.AccountViewModelImpl

@AndroidEntryPoint
class AccountScreen:Fragment(R.layout.screen_account) {
    private val binding by viewBinding(ScreenAccountBinding::bind)
    private val viewModel:AccountViewModel by viewModels<AccountViewModelImpl>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
        viewModelListeners()
        Handler(Looper.myLooper()!!).postDelayed(object :Runnable{
            override fun run() {
                viewModel.getAccountInformation()
            }

        },1)
    }

    private fun viewModelListeners() = with(binding){

        viewModel.accountEmail.onEach {
            textAccountPoint.setText(it)
        }.launchIn(lifecycleScope)

        viewModel.accountName.onEach {
            textAccountName.setText(it)
        }.launchIn(lifecycleScope)

        viewModel.accountImage.onEach {bitmap->
            if (bitmap!=null){
                image.setImageBitmap(bitmap)
                add.visibility=View.GONE
            }
        }.launchIn(lifecycleScope)
    }

    private fun listeners()= with(binding) {
        accountImage.setOnClickListener {
            findNavController().navigate(R.id.action_mainScreen_to_profileScreen)
        }
        addressManagement.setOnClickListener {
            findNavController().navigate(R.id.action_mainScreen_to_addressScreen)
        }
        paymentManagement.setOnClickListener {
            findNavController().navigate(R.id.action_mainScreen_to_paymentManegScreen)
        }
        yourPromotion.setOnClickListener {
            findNavController().navigate(R.id.action_mainScreen_to_yourPromotionScreen)
        }
        orderHistory.setOnClickListener {

        }
        logout.setOnClickListener {

        }
    }
}