package uz.infinityandro.foodorder.presenter.viewModel.account.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.presenter.viewModel.account.AddresAddViewModel
import uz.infinityandro.foodorder.util.Constants
import javax.inject.Inject

@HiltViewModel
class AddressAddViewModelImpl @Inject constructor(private val firestore: FirebaseFirestore):ViewModel(),AddresAddViewModel {
    override val progress= MutableSharedFlow<Boolean>()
    override val error= MutableSharedFlow<String>()
    override val success= MutableSharedFlow<Unit>()

    override fun saveDate(data: HashMap<String, Any>) {
        viewModelScope.launch {
            progress.emit(true)
        }
        firestore.collection(Constants.ADDRES)
            .add(data)
            .addOnSuccessListener {
                viewModelScope.launch {
                    progress.emit(false)
                    success.emit(Unit)
                }
            }.addOnFailureListener {
                viewModelScope.launch {
                    progress.emit(false)
                    error.emit("Something went wrong")
                }
            }
    }
}