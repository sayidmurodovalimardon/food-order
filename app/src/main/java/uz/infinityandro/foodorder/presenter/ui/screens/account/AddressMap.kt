package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.hilt.android.AndroidEntryPoint
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.PageAddressMapBinding
import uz.infinityandro.worldnews.utils.showToast

@AndroidEntryPoint
class AddressMap : Fragment(R.layout.page_address_map), OnMapReadyCallback {
    private val binding by viewBinding(PageAddressMapBinding::bind)
    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment

    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    var firstTime = true

    var previousLocation: Location? = null
    var currentLocation: Location? = null
    var long=0.0
    var lat=0.0
    var name=""
    var country=""



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        listeners()
        init()
    }


    private fun listeners() {
        binding.btnConfirm.setOnClickListener {
            val fragment=AddressAddMap()
            var bundle=Bundle()
            bundle.putDouble("long",long)
            bundle.putDouble("lan",lat)
            bundle.putString("name",name)
            bundle.putString("country",country)
            fragment.arguments=bundle
            findNavController().navigate(R.id.action_addressMap_to_addressAddMap,bundle)

        }

        binding.iconMap.setOnClickListener {
            var list: List<Address>? = null
            var geocoder = Geocoder(requireContext())
            list = geocoder.getFromLocation(currentLocation!!.latitude, currentLocation!!.longitude, 1)
            var address: Address = list!!.get(0)
            binding.trueLocation.setText(address.countryName)
            binding.kochasi.setText(address.featureName)
            val userLan = LatLng(currentLocation!!.latitude, currentLocation!!.longitude)
            lat=address.latitude
            long=address.longitude
            name=address.featureName
            country=address.countryName
            mMap.clear()
            mMap.addMarker(MarkerOptions().position(userLan).title("Your location"))
            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    userLan,
                    18f
                )
            )
        }

        binding.textDetail.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val location = binding.textDetail.query.toString()
                var list: List<Address>? = null
                if (location != null || !location.equals("")) {
                    try {
                        var geocoder = Geocoder(requireContext())
                        list = geocoder.getFromLocationName(location, 1)
                        var address: Address = list!!.get(0)
                        binding.trueLocation.setText(address.countryName)
                        binding.kochasi.setText(address.featureName)
                        val userLan = LatLng(address!!.latitude, address!!.longitude)
                        lat=address.latitude
                        long=address.longitude
                        name=address.featureName
                        country=address.countryName
                        mMap.clear()
                        mMap.addMarker(MarkerOptions().position(userLan).title("Your location"))
                        mMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                userLan,
                                18f
                            )
                        )
                    }catch (e: Exception) {
                        showToast("Can't find this location")
                    }

                }

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    @SuppressLint("MissingPermission")
    fun init() {

        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.fastestInterval = 3000
        locationRequest.interval = 5000
        locationRequest.smallestDisplacement = 10f

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                val newPos = LatLng(p0.lastLocation.latitude, p0.lastLocation.longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newPos, 18f))
                if (firstTime) {
                    previousLocation = p0.lastLocation
                    currentLocation = p0.lastLocation
                    firstTime = false
                } else {
                    previousLocation = currentLocation
                    currentLocation = p0.lastLocation
                }

            }

            override fun onLocationAvailability(p0: LocationAvailability) {
                super.onLocationAvailability(p0)
            }
        }

        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()!!
        )


    }

    fun chekPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (chekPermission()) {
            mMap.uiSettings.isMyLocationButtonEnabled = true
            mMap.setOnMyLocationClickListener {

                fusedLocationProviderClient.lastLocation
                    .addOnFailureListener {

                    }.addOnSuccessListener { succes ->
                        var list: List<Address>? = null
                        var geocoder = Geocoder(requireContext())
                        list = geocoder.getFromLocation(succes.latitude,succes.longitude, 1)
                        var address: Address = list!!.get(0)
                        lat=address.latitude
                        long=address.longitude
                        name=address.featureName
                        country=address.countryName
                        val userLan = LatLng(succes.latitude, succes.longitude)
                        mMap.clear()
                        mMap.addMarker(MarkerOptions().position(userLan).title("Your location"))
                        mMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                userLan,
                                18f
                            )
                        )

                    }
            }
        } else {
            Dexter.withContext(requireActivity())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    @SuppressLint("MissingPermission")
                    override fun onPermissionGranted(p0: PermissionGrantedResponse?) {

                    }

                    override fun onPermissionDenied(p0: PermissionDeniedResponse?) {

                    }

                    override fun onPermissionRationaleShouldBeShown(
                        p0: PermissionRequest?,
                        p1: PermissionToken?
                    ) {

                    }

                }).check()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        getLastLocation()

    }
}