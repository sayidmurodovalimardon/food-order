package uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.impl

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpCodeUseCase
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.UpCodeViewModel
import javax.inject.Inject

@HiltViewModel
class UpCodeViewModelImpl @Inject constructor(private val useCase: UpCodeUseCase) :ViewModel(),UpCodeViewModel {
    override val backFlow= MutableSharedFlow<Unit>()
    override val phoneNumberFlow= MutableSharedFlow<String>()

    override fun backFun() {
        viewModelScope.launch {
            backFlow.emit(Unit)
        }
    }

    override fun getPhoneNumber() {
        useCase.getPhone().onEach {
            it.onSuccess { phone->
                phoneNumberFlow.emit(phone)
            }.onFailure {

            }
        }.launchIn(viewModelScope)
    }
}