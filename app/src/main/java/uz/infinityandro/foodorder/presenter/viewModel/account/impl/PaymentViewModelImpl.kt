package uz.infinityandro.foodorder.presenter.viewModel.account.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.data.model.card.Card
import uz.infinityandro.foodorder.presenter.viewModel.account.PaymentViewModel
import uz.infinityandro.foodorder.util.Constants
import javax.inject.Inject

@HiltViewModel
class PaymentViewModelImpl @Inject constructor(private val firestore: FirebaseFirestore):ViewModel(),PaymentViewModel {
    override val cards= MutableSharedFlow<List<Card>>()
    override val progress= MutableSharedFlow<Boolean>()
    override val toCard= MutableSharedFlow<Unit>()
    override val list: ArrayList<Card> = ArrayList()

    override fun getData(){
        list.clear()
        viewModelScope.launch {
            progress.emit(true)
        }
        firestore.collection(Constants.CARD)
            .get()
            .addOnSuccessListener {result->
                result.onEach { document ->
                    val number = document["number"] as String
                    val name = document["name"] as String
                    val expiredDay = document["expiredDay"] as String

                    val data = Card(number, name, expiredDay)
                    list.add(data)

                }
                viewModelScope.launch {
                    cards.emit(list)
                    progress.emit(false)
                }
            }
    }

    override fun toFlow() {
        viewModelScope.launch {
            toCard.emit(Unit)
        }
    }

}