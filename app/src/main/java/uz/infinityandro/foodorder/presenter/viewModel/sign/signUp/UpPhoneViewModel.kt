package uz.infinityandro.foodorder.presenter.viewModel.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpPhoneViewModel {
    val backFlow:Flow<Unit>
    val singInFlow:Flow<Unit>
    val progressFlow:Flow<Boolean>
    val phoneNumberFlow:Flow<String>

    fun backFun()
    fun singInScreen()
    fun savePhoneNumber(phone:String)
}