package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.PageOrderHistoryBinding

@AndroidEntryPoint
class OrderHistoryScreen:Fragment(R.layout.page_order_history) {

    private val binding by viewBinding(PageOrderHistoryBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}