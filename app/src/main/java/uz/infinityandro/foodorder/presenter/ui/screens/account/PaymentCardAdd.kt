package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.PagePaymentCardAddBinding
import uz.infinityandro.foodorder.presenter.viewModel.account.PaymentAddCardViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.PaymentAddCardViewModelImpl

@AndroidEntryPoint
class PaymentCardAdd:Fragment(R.layout.page_payment_card_add) {
    private val binding by viewBinding(PagePaymentCardAddBinding::bind)
    private val viewModel:PaymentAddCardViewModel by viewModels<PaymentAddCardViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding){
        super.onViewCreated(view, savedInstanceState)
        combine(
            cardNumberEditText.textChanges().map {
                it.length==19
            },
            cardName.textChanges().map {
                it.length >0
            },
            cardExpiredDay.textChanges().map {
                it.length==5
            },
            transform = {cardNumberEditText,cardName, cardEpiredDay->
                cardEpiredDay && cardName && cardNumberEditText
            }
        ).onEach {
            btnAddCardConfirm.isEnabled=it
        }.launchIn(lifecycleScope)
        listeners()
    }

    private fun listeners() = with(binding){
        btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
        btnAddCardConfirm.setOnClickListener {
            val number=cardNumberEditText.rawText
            val name=cardName.text.toString()
            val day=cardExpiredDay.rawText
            viewModel.save(number,name,day)

            findNavController().popBackStack()

        }
        btnScan.setOnClickListener {
        }
    }
}