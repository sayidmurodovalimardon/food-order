package uz.infinityandro.foodorder.presenter.ui.adapter.address

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.foodorder.data.model.Adress
import uz.infinityandro.foodorder.databinding.ItemAddressLocationBinding

class AdapterLocation(var list: List<Adress>):RecyclerView.Adapter<AdapterLocation.VH>() {
    inner  class VH(var bindig:ItemAddressLocationBinding):RecyclerView.ViewHolder(bindig.root){
        fun bind(adress: Adress) = with(bindig){

            home.setText(adress.addressName)
            location.setText(adress.name)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemAddressLocationBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}