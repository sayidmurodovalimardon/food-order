package uz.infinityandro.foodorder.presenter.viewModel.see_all

import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import uz.infinityandro.foodorder.presenter.ui.screens.see_all.event.ResUIEvent
import uz.infinityandro.foodorder.presenter.ui.screens.see_all.state.ResInfoState


interface ResVM {

    val state: StateFlow<ResInfoState>

    val event: SharedFlow<ResUIEvent>

}