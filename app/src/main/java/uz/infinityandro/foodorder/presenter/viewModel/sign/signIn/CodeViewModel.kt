package uz.infinityandro.foodorder.presenter.viewModel.sign.signIn

import kotlinx.coroutines.flow.Flow

interface CodeViewModel {
    val backFlow: Flow<Unit>
    val phoneNumberFlow: Flow<String>

    fun backFun()
    fun getPhoneNumber()
}