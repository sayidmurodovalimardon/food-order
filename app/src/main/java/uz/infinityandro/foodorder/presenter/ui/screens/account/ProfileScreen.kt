package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.github.dhaval2404.imagepicker.ImagePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.app.App
import uz.infinityandro.foodorder.databinding.PageProfileBinding
import uz.infinityandro.foodorder.presenter.viewModel.account.ProfileViewModel
import uz.infinityandro.foodorder.presenter.viewModel.account.impl.ProfileViewModelImpl
import java.io.ByteArrayOutputStream
import java.io.InputStream


@AndroidEntryPoint
class ProfileScreen : Fragment(R.layout.page_profile) {
    private val binding by viewBinding(PageProfileBinding::bind)
    private val viewModel: ProfileViewModel by viewModels<ProfileViewModelImpl>()
    lateinit var image: Bitmap

    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding) {
        super.onViewCreated(view, savedInstanceState)

        Handler(Looper.myLooper()!!).postDelayed(object :Runnable{
            override fun run() {
                viewModel.getAccountInformation()
            }

        },10)
        listeners()
        viewModelListenrs()
    }

    private fun listeners() = with(binding) {
        accountImage.setOnClickListener {

            ImagePicker.with(this@ProfileScreen)
                .compress(1024)
                .maxResultSize(1080, 1080)
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
        }
        btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
        btnSave.setOnClickListener {
            viewModel.saveDate(
                image,
                accountName.text.toString(),
                accountEmail.text.toString(),
                "+${accountPhone.rawText}",
                accountPassword.text.toString()
            )
            findNavController().popBackStack()
        }
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!

                binding.accountImage.setImageURI(fileUri)
                val bitmap: Bitmap = binding.accountImage.drawable.toBitmap()
                image=bitmap
            } else if (resultCode == ImagePicker.RESULT_ERROR) {

            } else {

            }
        }
    private fun getInputStream(image: Uri): InputStream? {
        return App.instance.contentResolver.openInputStream(image)
    }
    private fun viewModelListenrs() = with(binding) {

        viewModel.progressFlow.onEach {
            if (it) {
                progress.visibility = View.VISIBLE
                btnSave.visibility = View.GONE
            } else {
                progress.visibility = View.GONE
                btnSave.visibility = View.VISIBLE
            }
        }.launchIn(lifecycleScope)

        viewModel.accountEmail.onEach {
            textAccountPoint.text = it
            accountEmail.setText(it)
        }.launchIn(lifecycleScope)

        viewModel.accountImage.onEach {
            if (it != null){
                image=it
                accountImage.setImageBitmap(it)
            }
        }.launchIn(lifecycleScope)

        viewModel.accountName.onEach {
            textAccountName.text = it
            accountName.setText(it)
        }.launchIn(lifecycleScope)

        viewModel.accountPassword.onEach {
            accountPassword.setText(it)
        }.launchIn(lifecycleScope)

        viewModel.accountPhone.onEach {
            accountPhone.setText(it)
        }.launchIn(lifecycleScope)

    }

}