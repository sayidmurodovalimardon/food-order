package uz.infinityandro.foodorder.presenter.viewModel.home.impl

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.data.model.Category
import uz.infinityandro.foodorder.data.model.Food
import uz.infinityandro.foodorder.data.model.Promotion
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.presenter.viewModel.home.HomeViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModelImpl @Inject constructor(private val firestore: FirebaseFirestore) : ViewModel(),
    HomeViewModel {
    override val list: ArrayList<Restaurant> = ArrayList()
    override val listFod: ArrayList<Food> = ArrayList()
    override val progressRestaran = MutableSharedFlow<Boolean>()
    override val joylashuv = MutableSharedFlow<String>()
    override val restaran = MutableSharedFlow<List<Restaurant>>()
    override val food = MutableSharedFlow<List<Food>>()
    override val progressFood = MutableSharedFlow<Boolean>()
    override val promotion = MutableSharedFlow<List<Promotion>>()
    override val categoryList = MutableSharedFlow<List<Category>>()


    override fun getLocation() {

    }

    override fun getRestaran() {
        list.clear()
        viewModelScope.launch {
            progressRestaran.emit(true)
        }
        firestore.collection("restaurants")
            .get()
            .addOnSuccessListener { result ->
                result.onEach { document ->
                    val id = document["id"] as String
                    val name = document["name"] as String
                    val image = document["image"] as String
                    val locationName = document["locationName"] as String
                    val long = document["long"] as Double
                    val lan = document["lan"] as Double
                    val isNav = document["isNav"] as Boolean
                    val rate = document["rate"] as Double
                    val user = Restaurant(id, name, image, locationName, long, lan, isNav, rate)
                    list.add(user)

                }
                viewModelScope.launch {
                    progressRestaran.emit(false)
                    restaran.emit(list)
                }
            }.addOnFailureListener {

            }

    }

    override fun getFoods() {
        viewModelScope.launch {
            progressFood.emit(true)
        }
        firestore.collection("foods")
            .get()
            .addOnSuccessListener { result ->
                result.onEach { document ->

                    if (document["foods"] != null) {
                        var hash: HashMap<String, Any> = document["foods"] as HashMap<String, Any>
                        val isFav = hash.get("_fav") as Boolean
                        val image = hash.get("image") as String
                        val cost = hash.get("cost") as Double
                        val rating = hash.get("rating") as Double
                        val count = hash.get("count").toString()
                        val name = hash.get("name") as String
                        val id = hash.get("id").toString()
                        val category = hash.get("category") as String
                        val data=Food(id,image,name,category,cost,count,isFav,rating)
                        listFod.add(data)
                    }
                }
                viewModelScope.launch {
                    progressFood.emit(false)
                    food.emit(listFod)

                }
            }.addOnFailureListener {

            }
    }

    override fun getPromotion() {
    }

    override fun getCategoryList() {
    }
}