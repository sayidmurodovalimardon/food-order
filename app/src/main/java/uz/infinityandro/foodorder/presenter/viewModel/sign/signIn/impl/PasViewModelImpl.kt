package uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.PasUseCase
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.PasViewModel
import javax.inject.Inject

@HiltViewModel
class PasViewModelImpl @Inject constructor(private val useCase:PasUseCase) :ViewModel(),PasViewModel {
    override val openScreen= MutableSharedFlow<Unit>()

    override fun savePassword(password: String) {

        useCase.savePassword(password).onEach {
            it.onSuccess {
                openScreen.emit(Unit)
            }

        }.launchIn(viewModelScope)

    }

}