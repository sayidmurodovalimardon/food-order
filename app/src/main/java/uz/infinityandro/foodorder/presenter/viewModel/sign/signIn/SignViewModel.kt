package uz.infinityandro.foodorder.presenter.viewModel.sign.signIn

import kotlinx.coroutines.flow.Flow

interface SignViewModel {
    val backFlow:Flow<Unit>
    val accountNew:Flow<Unit>
    val progress:Flow<Boolean>
    val resetPassword:Flow<Unit>
    val checkUser:Flow<Boolean>

    fun back()
    fun newAccount()
    fun resetPassword()
    fun checkUser(phone:String,password:String)

}