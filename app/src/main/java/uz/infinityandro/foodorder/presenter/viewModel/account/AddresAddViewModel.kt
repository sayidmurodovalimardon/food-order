package uz.infinityandro.foodorder.presenter.viewModel.account

import kotlinx.coroutines.flow.Flow

interface AddresAddViewModel {
    val progress:Flow<Boolean>
    val error:Flow<String>
    val success:Flow<Unit>
    fun saveDate(data:HashMap<String,Any>)
}