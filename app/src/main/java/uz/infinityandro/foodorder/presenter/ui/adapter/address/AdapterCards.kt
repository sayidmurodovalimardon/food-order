package uz.infinityandro.foodorder.presenter.ui.adapter.address

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.foodorder.data.model.card.Card
import uz.infinityandro.foodorder.databinding.ItemCardsBinding

class AdapterCards(var list:ArrayList<Card>):RecyclerView.Adapter<AdapterCards.VH>() {
    inner class VH(val binding:ItemCardsBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(card: Card)= with(binding) {
            home.setText(card.name)
            cardNumber.setText(card.number)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemCardsBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}