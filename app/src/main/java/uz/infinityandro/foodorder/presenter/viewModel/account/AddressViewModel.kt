package uz.infinityandro.foodorder.presenter.viewModel.account

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.Adress

interface AddressViewModel {
    val progress:Flow<Boolean>
    val list:List<Adress>
    val data:Flow<List<Adress>>

    fun getData()
}