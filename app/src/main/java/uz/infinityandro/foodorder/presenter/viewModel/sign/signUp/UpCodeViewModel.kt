package uz.infinityandro.foodorder.presenter.viewModel.sign.signUp

import kotlinx.coroutines.flow.Flow

interface UpCodeViewModel {
    val backFlow: Flow<Unit>
    val phoneNumberFlow: Flow<String>

    fun backFun()
    fun getPhoneNumber()
}