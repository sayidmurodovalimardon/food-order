package uz.infinityandro.foodorder.presenter.viewModel.sign.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import uz.infinityandro.foodorder.data.pref.MyPref
import uz.infinityandro.foodorder.presenter.viewModel.sign.GetStartViewModel
import javax.inject.Inject

@HiltViewModel
class GetViewModelImpl @Inject constructor(private val pref: MyPref) :ViewModel(),GetStartViewModel {
    override val createAccountFlow= MutableSharedFlow<Unit>()
    override val signInFlow= MutableSharedFlow<Unit>()
    override val googleOpenFlow= MutableSharedFlow<Unit>()

    override fun accountScreen() {
        viewModelScope.launch {
            createAccountFlow.emit(Unit)
        }
    }

    override fun signInScreen() {
        viewModelScope.launch {
            signInFlow.emit(Unit)
        }
    }

    override fun googleScreen(name:String,email:String) {
        viewModelScope.launch {
            googleOpenFlow.emit(Unit)
        }
        pref.name=name
        pref.email=email

    }
}