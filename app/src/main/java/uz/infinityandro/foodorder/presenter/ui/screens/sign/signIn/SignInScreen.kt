package uz.infinityandro.foodorder.presenter.ui.screens.sign.signIn

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.widget.textChanges
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenSignInBinding
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.SignViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.signIn.impl.SignViewModelImpl

@AndroidEntryPoint
class SignInScreen:Fragment(R.layout.screen_sign_in) {
    private val binding by viewBinding(ScreenSignInBinding::bind)
    private val viewModel:SignViewModel by viewModels<SignViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding){
        super.onViewCreated(view, savedInstanceState)
        listeners()
        viewModelisteners()
        combine(
            etPhoneNumber.textChanges().map {
                it.length==19
            },
            etPassword.textChanges().map {
                it.length==4
            },
            transform = {etPhoneNumber,etPassword->
                etPassword && etPhoneNumber
            }
        ).onEach {
            btConfirm.isEnabled=it
        }.launchIn(lifecycleScope)
    }

    private fun listeners() = with(binding){
        btBack.setOnClickListener {
            viewModel.back()
        }
        btConfirm.setOnClickListener {
            val phone="+998${etPhoneNumber.rawText}"
            val  code=etPassword.text.toString()
            viewModel.checkUser(phone,code)

        }
        createAccount.setOnClickListener {
            viewModel.newAccount()
        }
        resetPassword.setOnClickListener {
            viewModel.resetPassword()
        }

    }

    private fun viewModelisteners() = with(binding){
        viewModel.resetPassword.onEach {
            findNavController().navigate(R.id.resetPaswordPhone)
        }.launchIn(lifecycleScope)

        viewModel.backFlow.onEach {
            findNavController().popBackStack()
        }.launchIn(lifecycleScope)

        viewModel.accountNew.onEach {
            findNavController().navigate(R.id.signUpPhone)
        }.launchIn(lifecycleScope)

        viewModel.progress.onEach {
            progress.isVisible=it
            btConfirm.isVisible=!it
        }.launchIn(lifecycleScope)

        viewModel.checkUser.onEach {
            if (it){
                findNavController().navigate(R.id.pinCodeScreen)
            }else{
                etPassword.error="No user found"
            }
        }.launchIn(lifecycleScope)
    }
}