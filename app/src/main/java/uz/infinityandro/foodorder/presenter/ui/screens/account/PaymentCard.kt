package uz.infinityandro.foodorder.presenter.ui.screens.account

import android.os.Bundle
import android.view.View
import androidx.camera.core.Preview
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.PagePaymentCardBinding

@AndroidEntryPoint
class PaymentCard : Fragment(R.layout.page_payment_card) {
    private val binding by viewBinding(PagePaymentCardBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btn.setOnClickListener {

        }

    }

    private fun buildPreview(): Preview = Preview.Builder()
        .build()
        .apply {
        }
}