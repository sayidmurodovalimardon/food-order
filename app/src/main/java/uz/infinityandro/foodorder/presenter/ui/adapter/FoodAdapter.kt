package uz.infinityandro.foodorder.presenter.ui.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import uz.infinityandro.foodorder.app.App
import uz.infinityandro.foodorder.data.model.Food
import uz.infinityandro.foodorder.databinding.ItemFoodBinding
import uz.infinityandro.foodorder.databinding.ItemRestaurantBinding

class FoodAdapter(var list: List<Food>,var listener:(model:Food)->Unit):RecyclerView.Adapter<FoodAdapter.VH>() {
    inner class VH(var binding:ItemRestaurantBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(food: Food) = with(binding){
            binding.root.setOnClickListener {
                listener(food)
            }
            Glide.with(App.instance).load(food.image)
                .addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                }).into(imageFood)

            nameFood.setText(food.name)
            nameCategory.setText(food.category)
            ratingFood.setText(food.rating.toString())
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemRestaurantBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return  list.size
    }
}