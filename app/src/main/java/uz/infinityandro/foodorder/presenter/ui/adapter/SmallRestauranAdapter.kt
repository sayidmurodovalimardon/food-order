package uz.infinityandro.foodorder.presenter.ui.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import uz.infinityandro.foodorder.app.App
import uz.infinityandro.foodorder.data.model.Restaurant
import uz.infinityandro.foodorder.databinding.ItemRestaurantSmallBinding

class SmallRestauranAdapter(val list: List<Restaurant>, var listener: (model: Restaurant) -> Unit) :
    RecyclerView.Adapter<SmallRestauranAdapter.VH>() {
    inner class VH(var binding: ItemRestaurantSmallBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(restaurant: Restaurant) = with(binding) {
            binding.root.setOnClickListener {
                listener(restaurant)
            }
            Glide.with(App.instance).load(restaurant.image)
                .addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                }).into(imageFood)

            ratingFood.setText(restaurant.rating.toString())
            resName.setText(restaurant.name)
            resLocation.setText(restaurant.location_name)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            ItemRestaurantSmallBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}