package uz.infinityandro.foodorder.presenter.viewModel.onBoarding

import kotlinx.coroutines.flow.Flow

interface BoardViewModel {
    val openLoginScreenFlow : Flow<Boolean>
    fun open()
}