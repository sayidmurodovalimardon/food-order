package uz.infinityandro.foodorder.presenter.ui.screens.sign

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenGetStartedBinding
import uz.infinityandro.foodorder.presenter.viewModel.sign.GetStartViewModel
import uz.infinityandro.foodorder.presenter.viewModel.sign.impl.GetViewModelImpl

@AndroidEntryPoint
class GetStartedScreen:Fragment(R.layout.screen_get_started) {
    private val binding by viewBinding(ScreenGetStartedBinding::bind)
    private val viewModel:GetStartViewModel by viewModels<GetViewModelImpl>()
    private val firebaseAuth=FirebaseAuth.getInstance()
    private lateinit var googleSignInClient: GoogleSignInClient
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity()){}
        listeners()
        viewModelListeners()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)


    }
    companion object{
        private const val RC_SIGN_IN=1212
    }
    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
            }
        }
    }
    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    val user = firebaseAuth.currentUser
                    viewModel.googleScreen(user?.displayName.toString(),user?.email.toString())
                } else {
                }
            }
    }
    private fun viewModelListeners() {
        viewModel.createAccountFlow.onEach {
        findNavController().navigate(R.id.signUpPhone)
        }.launchIn(lifecycleScope)

        viewModel.signInFlow.onEach {
            findNavController().navigate(R.id.signInScreen)
        }.launchIn(lifecycleScope)

        viewModel.googleOpenFlow.onEach {
            findNavController().navigate(R.id.resetPasswordScreen)
        }.launchIn(lifecycleScope)

    }

    private fun listeners() = with(binding){
        btnNewAccount.setOnClickListener {
            viewModel.accountScreen()
        }
        btnSignIn.setOnClickListener {
            viewModel.signInScreen()
        }
        btnGoogle.setOnClickListener {
            signIn()
        }

    }
}