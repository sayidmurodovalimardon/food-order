package uz.infinityandro.foodorder.presenter.ui.screens.onBoarding

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.R
import uz.infinityandro.foodorder.databinding.ScreenOnboardingBinding
import uz.infinityandro.foodorder.presenter.ui.adapter.AdapterOnBoard
import uz.infinityandro.foodorder.presenter.viewModel.onBoarding.BoardViewModel
import uz.infinityandro.foodorder.presenter.viewModel.onBoarding.impl.BoardViewModelImpl

@AndroidEntryPoint
class OnBoardingScreen:Fragment(R.layout.screen_onboarding) {
    private val binding by viewBinding(ScreenOnboardingBinding::bind)
    private var firebaseAuth=FirebaseAuth.getInstance()
    private var adapter=AdapterOnBoard()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val user=firebaseAuth.currentUser
        if (user!=null){
            findNavController().navigate(R.id.action_onBoardingScreen_to_pinCodeScreen)
        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding){
        viewPager.adapter=adapter
        viewPager.isUserInputEnabled=false
        var count=viewPager.currentItem
        skip.setOnClickListener {
//            findNavController().navigate(R.id.action_onBoardingScreen_to_restaurants)
//            findNavController().navigate(R.id.action_onBoardingScreen_to_signGraph)
        }
        btnNext.setOnClickListener {
            count++
            viewPager.currentItem=++viewPager.currentItem
            if (count==2){
                btnNext.setText(getString(R.string.get_satrted))
            }
            if (count==3){
                findNavController().navigate(R.id.action_onBoardingScreen_to_signGraph)
            }
        }
    }
}