package uz.infinityandro.foodorder.presenter.viewModel.home

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.foodorder.data.model.Category
import uz.infinityandro.foodorder.data.model.Food
import uz.infinityandro.foodorder.data.model.Promotion
import uz.infinityandro.foodorder.data.model.Restaurant

interface HomeViewModel {
    val list :ArrayList<Restaurant>
    val listFod :ArrayList<Food>


    val progressRestaran:Flow<Boolean>
    val joylashuv:Flow<String>
    val restaran:Flow<List<Restaurant>>
    val food:Flow<List<Food>>
    val progressFood:Flow<Boolean>
    val promotion:Flow<List<Promotion>>
    val categoryList:Flow<List<Category>>

    fun getLocation()
    fun getRestaran()
    fun getFoods()
    fun getPromotion()
    fun getCategoryList()

}