package uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.impl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpPasUseCase
import uz.infinityandro.foodorder.presenter.viewModel.sign.signUp.UpPasViewModel
import javax.inject.Inject

@HiltViewModel
class UpPasViewModelImpl @Inject constructor(private val useCase:UpPasUseCase) :ViewModel(),UpPasViewModel {
    override val openScreen= MutableSharedFlow<Unit>()

    override fun savePassword(password: String) {

        useCase.savePassword(password).onEach {
            it.onSuccess {
                openScreen.emit(Unit)
            }

        }.launchIn(viewModelScope)

    }
}