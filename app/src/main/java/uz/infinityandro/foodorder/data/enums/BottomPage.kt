package uz.infinityandro.foodorder.data.enums

enum class BottomPage(val pos:Int) {

    HOME(0),
    LIKE(1),
    CARD(2),
    ACCOUNT(3)
}