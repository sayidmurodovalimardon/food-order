package uz.infinityandro.foodorder.data.model

data class Promotion (
    val image: Int,
    val name: String,
    val offer: String
)