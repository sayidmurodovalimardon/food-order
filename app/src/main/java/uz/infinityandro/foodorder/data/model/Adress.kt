package uz.infinityandro.foodorder.data.model

data class Adress(
    val addressName:String,
    val long:Double,
    val lan:Double,
    val name:String,
    val country:String
)
