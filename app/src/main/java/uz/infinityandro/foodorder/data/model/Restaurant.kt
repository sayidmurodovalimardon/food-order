package uz.infinityandro.foodorder.data.model

data class Restaurant(
    val id: String,
    val name: String,
    val image: String,
    val location_name: String,
    val long_map: Double,
    val lat_map: Double,
    val is_fav: Boolean,
    val rating: Double
)
