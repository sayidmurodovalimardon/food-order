package uz.infinityandro.foodorder.data.model

data class Food(
    val id: String,
    val image: String,
    val name: String,
    val category: String,
    val cost: Double,
    val count: String,
    val is_fav: Boolean,
    val rating: Double
)
