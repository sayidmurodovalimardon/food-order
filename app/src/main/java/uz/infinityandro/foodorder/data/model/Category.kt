package uz.infinityandro.foodorder.data.model

data class Category(
    val id: Int,
    val name: String,
    val image: String
)