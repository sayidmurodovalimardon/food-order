package uz.infinityandro.foodorder.data.model.card

data class Card(
    var number:String,
    var name:String,
    var expiredDay:String,
    var code:String?=""
)
