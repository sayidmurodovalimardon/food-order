package uz.infinityandro.foodorder.data.enums

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import uz.infinityandro.foodorder.R

enum class Start(
    @DrawableRes var image: Int,
    @StringRes var title: Int,
    @StringRes var description: Int

) {
    BIR(R.drawable.location,R.string.restaran,R.string.restarandescription),
    IKKI(R.drawable.directions,R.string.delivery,R.string.deliverydesc),
    UCH(R.drawable.wallet,R.string.wallet,R.string.walletdesc)

}