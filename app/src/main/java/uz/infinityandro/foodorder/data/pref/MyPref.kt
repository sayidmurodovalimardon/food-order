package uz.infinityandro.foodorder.data.pref

import android.content.Context
import android.content.Context.MODE_PRIVATE
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyPref @Inject constructor(private val context: Context) {

    private val pref = context.getSharedPreferences("cache1", MODE_PRIVATE)

    var name: String
        set(value) = pref.edit().putString("name", value).apply()
        get() = pref.getString("name", "")!!

    var phone: String
        set(value) = pref.edit().putString("phone", value).apply()
        get() = pref.getString("phone", "")!!

    var password: String
        set(value) = pref.edit().putString("password", value).apply()
        get() = pref.getString("password", "")!!

    var email: String
        set(value) = pref.edit().putString("email", value).apply()
        get() = pref.getString("email", "")!!

    var image: String
        set(value) = pref.edit().putString("image", value).apply()
        get() = pref.getString("image", "")!!

    var pos: Int
        set(value) = pref.edit().putInt("pos", value).apply()
        get() = pref.getInt("pos", 0)!!



}