package uz.infinityandro.foodorder.di

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uz.infinityandro.foodorder.domain.repository.see_all.MainRepo
import uz.infinityandro.foodorder.domain.repository.account.AccountRepository
import uz.infinityandro.foodorder.domain.repository.account.AddCardRepository
import uz.infinityandro.foodorder.domain.repository.account.PaymentRepository
import uz.infinityandro.foodorder.domain.repository.account.ProfileRepository
import uz.infinityandro.foodorder.domain.repository.account.impl.AccountRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.account.impl.AddCardRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.account.impl.PaymentRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.account.impl.ProfileRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.main.home.HomeRepository
import uz.infinityandro.foodorder.domain.repository.main.home.impl.HomeRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.see_all.imp.MainRepoImp
import uz.infinityandro.foodorder.domain.repository.sign.signIn.CodeRepository
import uz.infinityandro.foodorder.domain.repository.sign.signIn.PasRepository
import uz.infinityandro.foodorder.domain.repository.sign.signIn.PhoneRepository
import uz.infinityandro.foodorder.domain.repository.sign.signIn.SignRepository
import uz.infinityandro.foodorder.domain.repository.sign.signIn.impl.CodeRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.sign.signIn.impl.PasRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.sign.signIn.impl.PhoneRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.sign.signIn.impl.SignRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpCodeRepository
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpPasRepository
import uz.infinityandro.foodorder.domain.repository.sign.signUp.UpPhoneRepository
import uz.infinityandro.foodorder.domain.repository.sign.signUp.impl.UpCodeRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.sign.signUp.impl.UpPasRepositoryImpl
import uz.infinityandro.foodorder.domain.repository.sign.signUp.impl.UpPhoneRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun getSignUpPhone(impl:UpPhoneRepositoryImpl):UpPhoneRepository

    @Binds
    @Singleton
    abstract fun getSignUpCode(impl:UpCodeRepositoryImpl):UpCodeRepository

    @Binds
    @Singleton
    abstract fun getSignUpPas(impl:UpPasRepositoryImpl):UpPasRepository

    @Binds
    @Singleton
    abstract fun getSingIn(impl:SignRepositoryImpl):SignRepository

    @Binds
    @Singleton
    abstract fun getSignInPhone(impl:PhoneRepositoryImpl):PhoneRepository

    @Binds
    @Singleton
    abstract fun getSignInCode(impl:CodeRepositoryImpl):CodeRepository

    @Binds
    @Singleton
    abstract fun getSignInPas(impl:PasRepositoryImpl):PasRepository

    @Binds
    @Singleton
    abstract fun getAccountScreen(impl:AccountRepositoryImpl):AccountRepository

    @Binds
    @Singleton
    abstract fun getProfileScreen(impl:ProfileRepositoryImpl):ProfileRepository

    @Singleton
    @Binds
    abstract fun context(context: Context): Context

    @Binds
    @Singleton
    abstract fun getPayment(impl:PaymentRepositoryImpl):PaymentRepository

    @Binds
    @Singleton
    abstract fun getPaymentCard(impl:AddCardRepositoryImpl):AddCardRepository

    @Binds
    @Singleton
    abstract fun getMainInfo(repo: MainRepoImp): MainRepo





}