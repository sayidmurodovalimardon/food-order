package uz.infinityandro.foodorder.di

import android.content.Context
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uz.infinityandro.foodorder.data.pref.MyPref
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalModule {

    @Provides
    @Singleton
    fun getSharedPref(@ApplicationContext context: Context):MyPref=MyPref(context)

    @Provides
    @Singleton
    fun getDatabase()=FirebaseFirestore.getInstance()


}