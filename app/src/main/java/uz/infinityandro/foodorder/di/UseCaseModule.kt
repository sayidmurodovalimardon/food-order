package uz.infinityandro.foodorder.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uz.infinityandro.foodorder.domain.usecase.account.AccountUseCase
import uz.infinityandro.foodorder.domain.usecase.account.AddCardUseCase
import uz.infinityandro.foodorder.domain.usecase.account.PaymentUseCase
import uz.infinityandro.foodorder.domain.usecase.account.ProfileUseCase
import uz.infinityandro.foodorder.domain.usecase.account.impl.AccountUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.account.impl.AddCardUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.account.impl.PaymentUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.account.impl.ProfileUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.see_all.GetRestaurants
import uz.infinityandro.foodorder.domain.usecase.see_all.imp.GetRestaurantsImp
import uz.infinityandro.foodorder.domain.usecase.home.HomeUseCase
import uz.infinityandro.foodorder.domain.usecase.home.impl.HomeUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.CodeUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.PasUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.PhoneUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.SignUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl.CodeUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl.PasUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl.PhoneUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signIn.impl.SignUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpCodeUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpPasUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.UpPhoneUseCase
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.impl.UpCodeUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.impl.UpPasUseCaseImpl
import uz.infinityandro.foodorder.domain.usecase.sign.signUp.impl.UpPhoneUseCaseImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCaseModule {

    @Binds
    @Singleton
    abstract fun getSingUpPhone(impl:UpPhoneUseCaseImpl):UpPhoneUseCase

    @Binds
    @Singleton
    abstract fun getSingUpCode(impl:UpCodeUseCaseImpl):UpCodeUseCase

    @Binds
    @Singleton
    abstract fun getSingUpPas(impl:UpPasUseCaseImpl):UpPasUseCase

    @Binds
    @Singleton
    abstract fun getSignIn(impl:SignUseCaseImpl):SignUseCase

    @Binds
    @Singleton
    abstract fun getSignInPhone(impl:PhoneUseCaseImpl):PhoneUseCase

    @Binds
    @Singleton
    abstract fun getSingInCode(impl:CodeUseCaseImpl):CodeUseCase

    @Binds
    @Singleton
    abstract fun getSignInPas(impl:PasUseCaseImpl):PasUseCase

    @Binds
    @Singleton
    abstract fun getAccountScreen(impl:AccountUseCaseImpl):AccountUseCase

    @Binds
    @Singleton
    abstract fun getProfileScreen(impl:ProfileUseCaseImpl):ProfileUseCase

    @Binds
    @Singleton
    abstract fun getPayment(impl:PaymentUseCaseImpl):PaymentUseCase

    @Binds
    @Singleton
    abstract fun getAddCardPayment(impl:AddCardUseCaseImpl):AddCardUseCase

    @Binds
    @Singleton
    abstract fun getRestaurants(impl: GetRestaurantsImp): GetRestaurants


}